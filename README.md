# custom_interactive_markers

Creation of a custom interactive marker for asking poses to a robot. 

Works with : 
* taking the tip of the arrow
* moving circles
* commanding speed through a joystick topic (ex : 3D mouse, or 2D mouse + wheel of mouse)