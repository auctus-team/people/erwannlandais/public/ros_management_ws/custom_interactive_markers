#!/usr/bin/env python3

## use https://github.com/ros-visualization/visualization_tutorials/blob/noetic-devel/interactive_marker_tutorials/scripts/basic_controls.py 
## for features!!

## to add : 3D mouse

## TODO : 
##  - Anti spam (2e node) : si rotX / rotY / rotZ == 0 ==> ne rien envoyer
## 

## ==> va retourner twist
## ==> application twist sur ref local de marqueur virtuel
## ==> (twist multiplié par coeff pr scaling : on suppose dc que coeff contient increment de tps,
## pas bsn de l'ajouter à l'équation)
##          * Limite max coeff en fonction de : 
##              - freq publication de souris 3D
##              - vitesse max / s (ex : max : 3.0 rad / s)
##                  ==> cMax = 3.0/(freq)
##                  ==> est capage ultime pour éviter casse, si nu_twist est trop sensible
##          * Scaling € [0 - 1] = cPoss, fixé via slider
##          * nu_twist obtenu via souris 3D, € [0 - 1]
##          * ==> nu = cPoss * cMax * nu_twist
## ==> comment obtenir frame à k+1 dans world, sachant frame à k dans world
## et twist appliqué en local?
## Piste 1 : 
## (Lynch et Park p 112 - 113 + https://github.com/NxRLab/ModernRobotics/blob/master/packages/MATLAB/mr/VecTose3.m)
## - Exprimer frame k+1 en local par rapport à frame à k en local + twist en local :
##      * T[loc]k+1_[loc]k = I * e (nu) = I * se3mat(nu)
## - Exprimer frame k+1 dans world à partir de frame k en local
##     * T[loc]k+1_w = T^loc[k]_w * T^loc[k+1]_loc[k]
## Piste 2 :
## - Exprimer twist de local à world (via matrice adjointe, voir https://github.com/NxRLab/ModernRobotics/blob/master/packages/MATLAB/mr/Adjoint.m)
##      * nu_world = [Ad_T[world->loc[k]]]*nu_loc = Adjoint(T^loc[k]_world)  * nu_loc
## - Appliquer twist_world à Tk_w
##      * Tloc[k+1]_world = T^loc[k]_world * e(nu_world) = T^loc[k]_world * se3mat(nu_world)

## use https://github.com/NxRLab/ModernRobotics/tree/master/packages/Python

import rospy
import numpy as np
import copy

import tf.transformations

from interactive_markers.interactive_marker_server import *
from interactive_markers.menu_handler import *
from visualization_msgs.msg import *
from geometry_msgs.msg import Point, Pose, Twist, Vector3
from sensor_msgs.msg import Joy
from tf.broadcaster import TransformBroadcaster

from std_msgs.msg import String, Float64
from std_srvs.srv import SetBool, SetBoolRequest

from tf_management_pkg.tf_utils import *

from ros_wrap.com_manager import *

from msg_srv_action_gestion.srv import SetString, SetStringRequest,  Float64MultiArraySrv, Float64MultiArraySrvRequest

from random import random
from math import sin

import modern_robotics as mr
import os

import csv
from pathlib import Path
from datetime import datetime, timezone

def utc_to_local(utc_dt):
    return utc_dt.replace(tzinfo=timezone.utc).astimezone(tz=None)

def rand( min_, max_ ):
    return min_ + random()*(max_-min_)

def makeBox( msg ):
    marker = Marker()

    marker.type = Marker.CUBE
    marker.scale.x = msg.scale * 0.45
    marker.scale.y = msg.scale * 0.45
    marker.scale.z = msg.scale * 0.45
    marker.color.r = 0.5
    marker.color.g = 0.5
    marker.color.b = 0.5
    marker.color.a = 1.0

    return marker

def makeVial( msg, scale = 10.0, arrow=False ):
    marker = Marker()
    if arrow: 
        marker.type = Marker.ARROW
        marker.scale.x = scale*0.06
        marker.scale.y = scale*0.016
        marker.scale.z = scale*0.016

    else:

        marker.type = Marker.CYLINDER
        marker.scale.x = scale*0.016
        marker.scale.y = scale*0.016
        marker.scale.z = scale*0.06
    marker.color.r = 0.5
    marker.color.g = 0.5
    marker.color.b = 0.5
    marker.color.a = 1.0

    return marker


def makeBoxControl( msg,scale, doArrow = False, doVial = True ):
    control =  InteractiveMarkerControl()
    control.always_visible = True
    if doVial:
        control.markers.append( makeVial(msg,scale,arrow=doArrow) )        
    else:
        control.markers.append( makeBox(msg) )
    msg.controls.append( control )
    return control

def normalizeQuaternion( quaternion_msg ):
    norm = quaternion_msg.x**2 + quaternion_msg.y**2 + quaternion_msg.z**2 + quaternion_msg.w**2
    s = norm**(-0.5)
    quaternion_msg.x *= s
    quaternion_msg.y *= s
    quaternion_msg.z *= s
    quaternion_msg.w *= s

    return(quaternion_msg)

class CIM():
    """!
    

    """

    def csvRecordTrigger(self,req):
        suc = True
        if self.canRec:
            if req.data:
                ## start recording
                rospy.loginfo("START RECORDING CSV")

                ## flush
                self.begTimeMode = []
                self.modeUsed = []
                self.inRecording = True
            else:
                if self.inRecording:
                    rospy.loginfo("STOP RECORDING CSV")

                    curTime = rospy.Time.now().to_sec()
                    self.begTimeMode.append(curTime)
                    self.modeUsed.append(self.curMode)
                    ## stop recording

                    ## create .csv for record
                    strTime = utc_to_local(datetime.utcnow()).strftime("%Y%m%d_%H%M")
                    path = self.csvRecordFolder+"/"+strTime + ".csv"

                    allLines = [ ["Time of beginning of mode (s)", "Selected mode"] ]
                    begTime = self.begTimeMode[0]
                    for k in range(len(self.begTimeMode)):
                        self.begTimeMode[k] = self.begTimeMode[k] - begTime


                    for k in range(len(self.modeUsed)-1):
                        allLines.append([self.begTimeMode[k], self.modeUsed[k] ])
                    allLines.append([" "," "])
                    allLines.append( ["Time of final mode (s)", "Selected mode"] )                
                    allLines.append([self.begTimeMode[-1], self.modeUsed[-1] ])

                    rospy.loginfo("Files saved in %s"%(path))
                    self.writeCSV(path, allLines)



                self.inRecording = False

        return(True,"")


    def writeCSV(self,csvPath,allLines):
        ## check if path to file exists
        allL = csvPath.split("/")
        gPath = ""
        for k in range(len(allL)-1):
            gPath += allL[k] + "/"
        # print(gPath)
        # exit()
        path = Path(gPath)
        path.mkdir(parents=True,exist_ok=True)     


        with open(csvPath, 'w', newline='') as csvfile:
            cWrite = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for row in allLines:
                cWrite.writerow(row)        



    def setPoseInRef(self,name,T, name_refG =""):
        """!
        
        Set a tf pose (into /tf topic)
        
        """
        self.server.applyChanges()
        msg = geometry_msgs.msg.TransformStamped()

        if name_refG == "":
            name_refG = self.originFrame
        msg.transform = T
        msg.child_frame_id = name
        msg.header.frame_id = name_refG
        msg.header.stamp = rospy.Time.now()

        # rospy.loginfo( s + ": pose changed")

        # rospy.loginfo("[x,y,z] : [%3.4f, %3.4f, %3.4f]"%(feedback.pose.position.x, feedback.pose.position.y, feedback.pose.position.z))
        # rospy.loginfo("[w,x,y,z] : [%3.4f, %3.4f, %3.4f, %3.4f]"%(feedback.pose.orientation.w, feedback.pose.orientation.x, feedback.pose.orientation.y, feedback.pose.orientation.z))
        self.tU.tfToStaticTS(msg)   

        return(True) 


    def setVialPose(self,P):
        self.server.applyChanges()
        #rospy.loginfo("Setup %s frame ok!"%self.vialName)
        msg = geometry_msgs.msg.TransformStamped()

        msg.transform = self.tU.P2T(P)
        msg.child_frame_id = self.vialName
        msg.header.frame_id = self.originFrame
        msg.header.stamp = rospy.Time.now()

        # rospy.loginfo( s + ": pose changed")

        # rospy.loginfo("[x,y,z] : [%3.4f, %3.4f, %3.4f]"%(feedback.pose.position.x, feedback.pose.position.y, feedback.pose.position.z))
        # rospy.loginfo("[w,x,y,z] : [%3.4f, %3.4f, %3.4f, %3.4f]"%(feedback.pose.orientation.w, feedback.pose.orientation.x, feedback.pose.orientation.y, feedback.pose.orientation.z))
        self.tU.tfToStaticTS(msg)   

        return(True) 
    
    def setVialPoseViaFlt(self,msg):
        self.twist = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])        
        P = self.tU.M2P( self.applyServerToTF(self.tU.L2M(msg.array.data))   )

        suc = self.setVialPose(P)

        return(suc)

    def applyServerToTF(self,M):
        """!
        
        Transform from baseVial to midVial
        M : in baseVial
        Mok : in midVial
        
        """
        Mok = M@self.serv2TFTransf
        #Mok = np.linalg.inv(self.serv2TFTransf)@M
        return(Mok)
    
    def applyTFToServer(self,M):
        """!
        
        Transform from  midVial to baseVial
        M : in midVial
        Mok : in baseVial
        
        """
        Mok = M@ np.linalg.inv(self.serv2TFTransf)
        #Mok = np.linalg.inv(self.serv2TFTransf)@M
        return(Mok)
    
    def get2DMouse(self,msg):
        L = [-msg.y, -msg.x, msg.z]
        self.anglesToMarker(L)


    def getJoy(self,msg):

        twistMsg = Twist()
        twistMsg.angular.x = msg.axes[3]
        twistMsg.angular.y = msg.axes[4]
        twistMsg.angular.z = msg.axes[5]
        twistMsg.linear.x = msg.axes[0]
        twistMsg.linear.y = msg.axes[1]
        twistMsg.linear.z = msg.axes[2]                                 
        self.getTwist(twistMsg)

        if self.asPolar:
            if ( (rospy.Time.now().to_sec() - self.lastTimeTwistChange) > self.thresTimeForTwistChange):            
                for k in range(len(msg.buttons)):
                    self.curButtons[k] = msg.buttons[k]
                self.changeTwistRef = True
                self.lastTimeTwistChange = rospy.Time.now().to_sec()


        else:
            if ( (len(msg.buttons) == 4) and (msg.buttons[2] == 1 or msg.buttons[3] == 1) ):
                if ( (rospy.Time.now().to_sec() - self.lastTimeTwistChange) > self.thresTimeForTwistChange):
                    self.changeTwistRef = True
                    self.lastTimeTwistChange = rospy.Time.now().to_sec()

    def getTwist(self,msg):

        allInvalid = True

        if ( abs(msg.linear.x) <= 1.0):
            self.twist[3] = msg.linear.x
            allInvalid = False
        # else:
        #     self.twist[3] = 0.0

        if ( abs(msg.linear.y) <= 1.0):
            self.twist[4] = msg.linear.y
            allInvalid = False
        # else:
        #     self.twist[4] = 0.0


        if ( abs(msg.linear.z) <= 1.0):
            self.twist[5] = msg.linear.z
            allInvalid = False
        # else:
        #     self.twist[5] = 0.0


        if ( abs(msg.angular.x) <= 1.0):

            val = msg.angular.x
            #if (not self.isTwistLocal and self.refCubeName == "Mdes_0_frame"):
            if self.isTwistLocal or (self.curButtonState not in ["POLAR","X_POL"] ):
                ## dirty add for this particular case, to control only Z axis in local
                val = 0
            else:
                allInvalid = False

            self.twist[0] = val


        # else:
        #     self.twist[0] = 0.0

        if ( abs(msg.angular.y) <= 1.0):
            val = msg.angular.y
            #if (not self.isTwistLocal and self.refCubeName == "Mdes_0_frame"):
            if self.isTwistLocal or (self.curButtonState not in ["POLAR","Y_POL"] ):
                ## dirty add for this particular case, to control only Z axis in local
                val = 0
            else:
                allInvalid = False                

            self.twist[1] = val

        # else:
        #     self.twist[1] = 0.0

        if ( abs(msg.angular.z) <= 1.0):
            val = msg.angular.z

            if self.isTwistLocal or (self.curButtonState not in ["POLAR","Z_POL"] ):
               val = 0
            else:
               allInvalid = False

            self.twist[2] = val

        # else:
        #     self.twist[2] = 0.0


        ## for debug only
        # if not self.twistDebug:
        #     self.twist = self.cMax * self.twistSensibility * self.twist

        #self.isTwistValid = not allInvalid
        if (not allInvalid):
            if not self.isTwistValid:
                self.isTwistValid = True
            if not self.twistDebug:
                ## twist : [rx,ry,rz,x,y,z]
                ## only keeps max Dim
                maxDim = -1
                maxVal = 0
                for i in range(3):
                    if abs(self.twist[i]) > maxVal:
                        maxVal = abs(self.twist[i])
                        maxDim = i
                if maxDim != -1:
                    for i in range(3):
                        if i != maxDim:
                            self.twist[i] = 0.0




                self.twist = self.cMax * self.twistSensibility * self.twist


        if allInvalid:
            if self.twistDebug:
                #rospy.loginfo("Invalid twist listened. Twist will therefore stay at : ")
                #print(self.twist)
                a = 1
        
        





    def setVialPoseViaStr(self,msg):
        """!

        reset ref_marker at R^{tfToCheck}_{self.originFrame}
         
        tfToCheck is set in msg)

        
        """

        #eturn(True)
        
        self.twist = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        tfToCheck = msg.data
        
        #rospy.loginfo("TF TO CHECK : %s"%(tfToCheck))

        #L,suc = self.tU.getPose(self.refG_marker,tfToCheck)
        L,suc = self.tU.getPose(self.originFrame,tfToCheck)

        if suc:

            ## for vial

            ## Pori : for /TF
            
            for i in range(3):
                self.posOri[i] = L[i]
            
                if not self.vialInWorld:
                    self.posOri[i] = 0.0

            Tori = self.tU.L2T(L)
            Pori = self.tU.L2P(L)
            

            ## P : for server
            MendW = self.tU.L2M(L)

            # ## added to add a coherency at start,
            # ## where vM_frame and recCubePose are the same
            # Mreal =  MendW @ self.M_oriFixed_oriTwist @ np.linalg.inv(self.serv2TFTransf) 
            # P = self.tU.M2P(Mreal)
            # Pori = self.tU.M2P( self.tU.P2M(P) @ self.serv2TFTransf )


            ## we want 
            #P = self.tU.M2P( self.applyServerToTF(self.tU.L2M(L))   )

            # origin
            #P = self.tU.L2P(L)   
            ## if all is working, reset refName and refNameCur at their origin orientation
            # T_ori_ref = self.tU.M2T( self.M_refTwist_world )

            # P_to_apply = self.tU.M2P(  np.dot(self.M_oriFixed_oriTwist, self.tU.L2M(  L2) ) )

            suc = self.setPoseInRef(self.refG_marker,Tori, self.originFrame)

            if self.vialInWorld:
                ## otherwise, R^{vialRG}_{A} is set equally to
                ## R^{originFrame}_{A} (such as R^{vialRG}_{world} = I)
                ## (however, still no translation)
                self.M_vialRG_A =  np.linalg.inv(MendW) 
                for k in range(3):
                    self.M_vialRG_A[k,3] = 0.0

                T_vialRG_A = self.tU.M2T( self.M_vialRG_A )
                suc = self.setPoseInRef(self.vialRG,T_vialRG_A, self.refG_marker)

            if suc:
                ## now that RG is reset, also reset all the objects depending on this referential
                ## quick modification to refG_marker (offset)
                # M_vial_world = MendW @ self.M_oriFixed_oriTwist
                # print(M_vial_A)
        

                # M_vial_vialRG = np.linalg.inv(self.M_vialRG_A) @ M_vial_A

                ## Poss 1 
                # M_vial_A = self.M_oriFixed_oriTwist
                # R_D0_vialRG =  np.linalg.inv(self.M_vialRG_A) @ M_vial_A

                # R_B0_A = np.identity(4)
                # R_C0_B0 = np.identity(4)

                # R_C0_A = R_B0_A @ R_C0_B0
                
                ## Poss 2
                a = self.long_angle
                R_B0_A = np.array([[np.cos(a), -np.sin(a), 0.0,0.0],
                                    [np.sin(a), np.cos(a), 0.0, 0.0],
                                    [0.0, 0.0, 1.0, 0.0],
                                    [0.0, 0.0, 0.0, 1.0]                                                               
                                    ])
                
                a = self.trans_angle
                R_C0_B0 = np.array([[np.cos(a), 0.0, np.sin(a),0.0],
                                    [0.0, 1.0, 0.0,0.0],
                                    [-np.sin(a), 0.0, np.cos(a),0.0],    
                                    [0.0, 0.0, 0.0, 1.0]                        
                                    ])      

                a = self.vial_angle
                R_D0_C0 = np.array([[np.cos(a), -np.sin(a), 0.0,0.0],
                                    [np.sin(a), np.cos(a), 0.0, 0.0],
                                    [0.0, 0.0, 1.0, 0.0],
                                    [0.0, 0.0, 0.0, 1.0]                                                               
                                    ])         
                R_C0_A = R_B0_A @ R_C0_B0

                R_D0_vialRG =  np.linalg.inv(self.M_vialRG_A) @ R_C0_A @ R_D0_C0

                self.markerAndRefUpdate(R_B0_A,R_C0_A,R_D0_vialRG)
                #self.arrowUpdate(M_vial_vialRG)


                if suc:
                    self.rebootToTf = True         
                    ## set to False to suppress bug
                    ## where moving Zaxis at start would change
                    ## UD/LR axis
                    self.recomputeTht = False
                    
                    self.server.applyChanges()
            else:
                rospy.loginfo("Error : server communication bug")
   
        else:
            rospy.loginfo("Error : frame %s not found",tfToCheck)
        
        return(suc)

    def updateTht(self,M_vial_refFrame = None, M_refGMov_refG = None):
        """!

        M_vial_refFrame : pose de vialName dans refCubeName
        M_refGMov_refG : pose de refG_marker dans refCubeNameMoving
        
        Goal of this method : 
            - reset Mdes_0_frame_twist such as the tip of the arrow is ALWAYS
            aligned with V_init

            - reset savedcubePos
        
        """
        if type(M_vial_refFrame) == type(None):
            ## get the transform
            ## ok
            L2,suc = self.tU.getPose(self.refCubeName,self.vialName)
            #print(self.refCubeName)
            if suc:
                M_vial_refFrame = self.tU.L2M(L2)
            else:
                return(False)

        if type(M_refGMov_refG) == type(None):
            ## get the transform
            L,suc = self.tU.getPose(self.refG_marker,self.refCubeTrans)
            if suc:
                M_refGMov_refG = self.tU.L2M(L)
            else:
                return(False)

        
        ## 1 : obtenir projeté d'axe de fiole sur plan X-Y : aF_cur
        axis = np.zeros((4,1))
        axis[2] = 1.0
        axis[3] = 1.0

        P = np.dot(M_vial_refFrame, axis)

        normal = np.zeros((4,))
        normal[2] = 1.0
        normal[3] = 1.0

        pt = np.zeros((3,))    

        # print(pt)
        # print(normal)            
        R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
        
        P_XY = np.dot(R,P)

        singRisk = False

        if abs(P_XY[1]) < 1e-3: ## ok with 1e-3
            singRisk = True
        # if abs(P_XY[1]) > 5e-4: ## ok with 1e-3
        if True:
            #print(P_XY[:3])
            tht = np.arccos( np.dot(self.V_init,P_XY[:3])/ np.linalg.norm(P_XY[:3] ) )
            
            tht = tht[0]
            ## corrige pbme d'exp sur [0, pi uniquement]

            
            if (np.dot( self.secAxis, P_XY[:3] )) >0:
                tht = -tht
            
            #print(tht)

            # if (np.dot( self.secAxis, P_XY[:3] )) <0:
            #     tht = 2*np.pi - tht
            
            
            M_rot_ori = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,-tht])

            newM_rG = np.dot(M_refGMov_refG,  M_rot_ori )

            ## compare refCubeName-xaxis before / after mvt,
            ## to prevent any singularity issue
            if singRisk:
                Xaxis = np.array([1.0,0.0,0.0,1.0])

                Xaxis_before = np.dot(M_refGMov_refG,Xaxis)[:3]
                Xaxis_before = Xaxis_before/np.linalg.norm(Xaxis_before)

                Xaxis_after = np.dot(newM_rG,Xaxis)[:3]
                Xaxis_after/np.linalg.norm(Xaxis_after)
                # print(Xaxis_after)
                # print(Xaxis_before)
                # print( np.max(newM_rG - M_refGMov_refG) )

                angle = np.arccos( np.dot(Xaxis_after,Xaxis_before)/(np.linalg.norm(Xaxis_after)*np.linalg.norm(Xaxis_before))  )
                # if angle > 0.2:
                #     print("angle computed : ", angle)
                if angle > np.pi/2:
                    #print("SING ISSUE DETECTED")
                    corrMat = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,-np.pi])
                    newM_rG = np.dot(corrMat, newM_rG )


                    #self.pauseCode = True






            ## check sign of Y-axis of newM_rG : if do not corresponds to init value,
            ## do a pi rotation
            ## autre poss : si à sing, et qu'on a diff d'angle entre
            ## ancien axe Y de Mdes_0_frame ds RG et nvel axe Y de Mdes_0_frame
            ## supérieur à 3pi/4 ==> on a eu inversion, à corriger
            # Yaxis = np.array([0.0,1.0,0.0,1.0])
            # Xaxis = np.array([1.0,0.0,0.0,1.0])
            # Yaxis_M = np.dot(newM_rG, Yaxis)

            # if np.sign( np.dot(Yaxis_M, Xaxis)  ) != self.secAxis_rGXAxis_Sign:
            #     print("WARNING : ANGLE ERROR. WILL CORRECT")
            if self.refCubeName != self.refCubeTrans:
                self.setPoseInRef(self.refCubeName,self.tU.M2T(newM_rG), self.refG_marker)

            self.setPoseInRef(self.refCubeTrans,self.tU.M2T(newM_rG), self.refG_marker)

            self.server.setPose(self.refCubeTrans, self.tU.M2P(newM_rG))       

            ## update recCubePose orientation
            newL,suc = self.tU.getPose(self.refCubeName,self.vialName)
            if suc:
                vialAxis_in_RTN = np.dot( self.tU.L2M(newL)  ,axis)
                #print(self.tU.M2Lrpy( self.tU.L2M(newL)   ))
                normal = np.zeros((4,))
                normal[1] = 1.0
                normal[3] = 1.0
                pt = np.zeros((3,))    
                        # print(pt)
                        # print(normal)            
                R = tf.transformations.projection_matrix(pt,normal, pseudo = False)

                vialAxis_in_RTN_on_XZ_plane = np.dot(R, vialAxis_in_RTN)

                ax2Check = np.array([1.0,0.0,0.0])
                tht2 = np.arccos( np.dot(ax2Check,vialAxis_in_RTN_on_XZ_plane[:3])/ np.linalg.norm(vialAxis_in_RTN_on_XZ_plane[:3] ) )
                
                tht2 = tht2[0]   
                # print(vialAxis_in_RTN_on_XZ_plane)
                # print(tht2)      

                if (vialAxis_in_RTN_on_XZ_plane[2] < 0 ):
                    tht2 =-tht2
                else: 
                    tht2 = tht2

                
                M_rot_ori = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,-tht2,0.0])

                ## otherwise, there is a weird inversion where vialAxis corresponds
                ## to recCubePose-Xaxis (whereas we would want it to be recCubePose-Zaxis)
                M_off = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,np.pi/2,0.0])

                mat2Apply = np.dot(M_off, M_rot_ori)

                suc = self.setPoseInRef( self.recordedCubePos, self.tU.M2T(mat2Apply), self.refCubeName  )


                self.needToChangeRTNM = True

                    #print(vialAxis_in_RTN)





    def __init__(self):




        self.server = None
        self.menu_handler = MenuHandler()

        rospy.init_node("custom_marker")
        nodeName = rospy.get_name()
        nodeName+="/"
        self.originFrame = rospy.get_param(nodeName+"origin_frame","world")  
        self.vialName = rospy.get_param(nodeName+"vial_name","vial")  
        print("ref used for simMark: ", self.vialName)
        ## 3 different refs : 

        ## all markers and all refs should be expressed in refG_marker (aka A)

        ## refTwistOriFixed : never moving, expressed in world, basis for other refTwist 
        ## refG_marker : A 
        ## vialRG : ref général uniquement pour la fiole. Utilisé de base pour pouvoir fixer le référentiel dans lequel
        ## le déplacement libre 3D devait être exprimé. N'a cependant pas trop de sens; à remplacer par refG_markers. Au
        ## besoin, modifier M^{refG_marker}_{originFrame} OU ajouter une transfo intermédiaire M^{refG_marker}_{vialRG} et M^{vialRG}_{originFrame} 
        ## refCubeName : can move, expressed in world, basis for vial name. Will 
        ## move according to left-right movements. : Bk
        ## refCubeNameMoving : will move according to circles, twist, and position of tip of vialName : Ck
        ## vialName : Dk

        ## Pour que ça marche : il faut que R^{B_{0}}_{A} = R(z,180)
        ## check que option bien rajoutée. Au bsn : sinon, on ajoute juste une transfo pr ça.
        self.updateVialPoseInRef = False

        ## recCubePos corresponds to Ck
        self.recordedCubePos = "recCubePose"

        self.sign_sinbk = 1.0


        self.tU = tfUtils(True)

        self.cM = comManager()
        #self.refTwistOriFixed= rospy.get_param(nodeName+"ref_master","Mdes_0_frame")  
        #self.refCubeName = self.refTwistOriFixed+"_twist"

        self.refCubeTrans = rospy.get_param(nodeName+"ref_trans","cubeTrans")  

        self.refCubeLong = rospy.get_param(nodeName+"ref_long","cubeLong")  

        print("reference frame for cube : ", self.refCubeTrans)
        self.isSim = rospy.get_param(nodeName+"isSim",False)  
        self.isTwistLocal = True
        self.asPolar = True

        ## test : do not differentiate /tf frame associated to cube virtual marker (refCubeName)
        ## and interactiveMarker frame associated with cube (refCubeNameMoving).
        ## Reason : finally, not really necessary.
        ## refCubeName = "virtCube"
        ## recordedCubePos = "recCubePose"

        self.typeMouse = rospy.get_param("/global_params/typeMouse","3D")


        self.refG_marker = "refG_markers"

        self.csvRecordFolder = rospy.get_param(nodeName+"csvFolder","")

        self.begTimeMode = []
        self.modeUsed = []

        ## possibles modes : 
        ##  - arrow_3d : prise de pointe
        ##  - circle_[UD/LR/AR] : cercle, mvt upDown / LeftRight / AxisRotation
        ##  - 3dMouse_[POLAR/ZPOL/XPOL/YPOL]_[UD/LR/AR] : souris3D, full/bloqué sur un axe, mvt upDown / LeftRight / AxisRotation

        self.curMode = ""
        self.inRecording = False

        self.canRec = (self.csvRecordFolder != "")


        ## reference referential for origin axis rotation

        self.vialInWorld = True
        #self.vialInWorld = False

        self.vialRG ="vialRG"
        if not self.vialInWorld:
            ## orientation of vial according to refG_marker
      
            roll_vialRG = rospy.get_param(nodeName+"roll_vRG",0.0)
            pitch_vialRG = rospy.get_param(nodeName+"pitch_vRG",0.0)
            yaw_vialRG = rospy.get_param(nodeName+"yaw_vRG",0.0)

        else:
            ## otherwise, R^{vialRG}_{A} is set equally to
            ## R^{originFrame}_{A} (such as R^{vialRG}_{world} = I)
            ## at this point, we will just set it as identity.
            roll_vialRG = 0.0
            pitch_vialRG = 0.0
            yaw_vialRG = 0.0

        T2 = self.tU.Lrpy2T([0.0,0.0,0.0,roll_vialRG,pitch_vialRG,yaw_vialRG])

        self.M_vialRG_A = self.tU.T2M(T2)
        self.tU.tfToStatic(T2, [self.refG_marker,"",self.vialRG])


        self.curButtons = [0.0,0.0,0.0,0.0]
        self.curButtonState = "POLAR"
        self.butState = {
            2 : "Z_POL",
            3: "Y_POL",
            0: "X_POL",
            1: "POLAR"
        }

        ## threshold value for singularity
        self.thresSing = 1e-1
        ## threshold value for computation in singularity
        ## (if inferior to this threshold, do not change
        ## R_Bk_A)
        self.thresCompute = 1e-1

        self.inputCheck = False


        self.recomputeTht = True
        self.tht = 0.0
        self.PrimAxisVialSign = 0

        ## computes whether you want at beginning the tip of
        ## the vial to go to the left (getToRight = False)
        ## or to the right (begToRight = True)
        self.begToRight = rospy.get_param(nodeName+"begToRight",True)  

        #self.begToRight = False



        self.nodeFreq = 200 ## 1000
        ## max velocity (rad / s)
        self.maxVel = 6.0 

        self.twistDebug = False

        ## tF used for resetting boundaries of robot
        ## It corresponds to a Tf at a certain place, with 
        ## the same orientation as R^{Dk}_{Ck} on the vial axis
        self.tFCorrName = rospy.get_param(nodeName+"tfCorrName","centerBoundTF2")
        ## It is defined according to refGmarker orientation
        ## (normally : should be rotation on X or Y axis )
        roll_tfCorr = rospy.get_param(nodeName+"roll_tfC",-1.57078)
        pitch_tfCorr = rospy.get_param(nodeName+"pitch_tfC",0.0)
        yaw_tfCorr = rospy.get_param(nodeName+"yaw_tfC",0.0)

        self.M_tfC_refG = self.tU.Lrpy2M([0.0,0.0,0.0,roll_tfCorr,pitch_tfCorr,yaw_tfCorr])


        ## ROS Server
        self.server = InteractiveMarkerServer("custom_marker")
        self.withVial = False
        self.counter = 0    

        self.centerOffset = 0.03
        
        ## threshold time to change twist referential (in sec)
        self.thresTimeForTwistChange = 0.150  #0.250
        self.lastTimeTwistChange = rospy.Time.now().to_sec()

        self.rate = rospy.Rate(self.nodeFreq) # 1000
        # create a timer to update the published transforms
        #rospy.Timer(rospy.Duration(0.01), self.frameCallback)


        # self.menu_handler.insert( "First Entry", callback=self.processFeedback )
        # self.menu_handler.insert( "Second Entry", callback=self.processFeedback )
        # sub_menu_handle = self.menu_handler.insert( "Submenu" )
        # self.menu_handler.insert( "First Entry", parent=sub_menu_handle, callback=self.processFeedback )
        # self.menu_handler.insert( "Second Entry", parent=sub_menu_handle, callback=self.processFeedback )
    
        
        # position = Point(-3, 3, 0)
        # self.make6DofMarker( False, InteractiveMarkerControl.NONE, position, True)
        # position = Point( 0, 3, 0)69-55
        # self.make6DofMarker( True, InteractiveMarkerControl.NONE, position, True)
        # position = Point(-3, 0, 0)
        # self.make6DofMarker( False, InteractiveMarkerControl.ROTATE_3D, position, False)

        self.withVial = True
        position = Point(3, -3, 0)
        #self.make6DofMarker( False, InteractiveMarkerControl.ROTATE_3D, position, False)

        self.serv2TFTransf = np.identity(4)

        self.savedRTNM = np.identity(4)

        ## fixed position (e.g no translation)
        self.fixed = True

        self.needToChangeRTNM = False

        self.twist = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.isTwistValid = False


        self.twistSensibility = 0.01


        self.cMax =  self.maxVel/self.nodeFreq
        ##debug
        #self.cMax = 1e2

        self.posOri = [0,0,0]

        self.useRealVial = True


        ## wait to find world ==> Mdes_0_frame
        # foundMdes2World = False
        # while not rospy.is_shutdown() and not foundMdes2World:
        #     L,foundMdes2World  = self.tU.getPose(self.originFrame,self.refTwistOriFixed)


        self.vialMarkerPoseInRG =  self.tU.Lrpy2P([0.0,0.0,0.0,0.0,0.0,0.0]   )
        
        ## tests
        ## roll, pitch: change trop fiole, ko
        ## yaw : pas trouvé d'angle permettant un controle simple
        #self.vialMarkerPoseInRG =  self.tU.Lrpy2P([0.0,0.0,0.0,0.0,0.0,-3.14]   )
        
        self.V_init = np.array([1.0,0.0,0.0])
        self.secAxis = np.array([0.0,1.0,0.0])

        self.secAxis_rGXAxis_Sign = -1
        # self.M_refTwistOri_world = self.tU.L2M(L)


        self.pauseCode = False
        ### Poss 1 :
        # ## create transform here for Mdes_0_frame -> Mdes_0_frame_twist (simpler)
        # roll = rospy.get_param(nodeName+"roll_rfm_t",0.0)
        # pitch = rospy.get_param(nodeName+"pitch_rfm_t",0.0)
        # yaw = rospy.get_param(nodeName+"yaw_rfm_t",0.0)
        # # roll = 0.0
        # # pitch = 0.0
        # # yaw = 1.57078
        # ## initial vector V for orientation, on plan X-Y

        # ## V_init : axis to which tip of vial should corresponds on plan X-Y,
        # ## in refG_marker


        # if not self.begToRight:
        #     roll = 0.0
        #     pitch = 0.0
        #     yaw = -1.57078
        

        # # self.ori_refTwist_transform = geometry_msgs.msg.TransformStamped()

        # T = self.tU.Lrpy2T([0.0,0.0,0.0,roll,pitch,yaw])

        ##self.M_oriFixed_oriTwist = self.tU.T2M(T)

        ## Poss 2 : Let set initial angles for each circle
        self.long_angle = rospy.get_param(nodeName+"long_angle",np.pi/2)
        self.trans_angle = rospy.get_param(nodeName+"trans_angle",0.0)
        self.vial_angle = rospy.get_param(nodeName+"vial_angle",-np.pi/2)
  
        #  pitch = rospy.get_param(nodeName+"pitch_rfm_t",0.0)
        # yaw = rospy.get_param(nodeName+"yaw_rfm_t",0.0)

        # self.M_refTwist_world = np.dot( self.M_refTwistOri_world,  self.tU.T2M(T) )

        self.makeVialMarker(position)



        self.server.applyChanges()



        
                
        # msg.transform.rotation.w = 1
        # msg.transform.rotation.x = 0
        # msg.transform.rotation.y = 0
        # msg.transform.rotation.z = 0

        # self.ori_refTwist_transform.child_frame_id = self.refCubeName
        # self.ori_refTwist_transform.header.frame_id = self.originFrame
        # self.ori_refTwist_transform.header.stamp = rospy.Time.now()
        # self.ori_refTwist_transform.transform = self.tU.M2T(self.M_refTwist_world)
        
        # self.tU.tfToStaticTS(self.ori_refTwist_transform)


        self.changeTwistRef = False
        if (self.typeMouse == "3D"):
            ## Subscriber
            #self.twist_sub = rospy.Subscriber("/spacenav/twist", Twist, self.getTwist)
            self.joy_sub = rospy.Subscriber("/spacenav/joy", Joy, self.getJoy)

            self.sensibility_sub = rospy.Subscriber("/3dMouseSensibility", Float64, self.getSensibility)
            ## Client
            self.reset_sensibility = rospy.ServiceProxy("/sensibility_reset", Float64MultiArraySrv)
            ## Return name of ref
            ## Special if name of ref is "LOCAL"
            self.change_twist_interface = rospy.ServiceProxy("/set_twist_kind", SetString)

            req = Float64MultiArraySrvRequest()
            req.array.data.append(self.twistSensibility)

            suc = False

            while not rospy.is_shutdown() and not suc:
                suc,resp,ret = self.cM.call_service("/sensibility_reset",self.reset_sensibility,req)

            self.changeTwistRef = True
        
        
        elif self.typeMouse == "2D":
               ## Subscriber
               self.mouse2D_sub = rospy.Subscriber("/2dmouse_XYZ", Vector3, self.get2DMouse)


        ## Server
        self.track_pose_str = rospy.Service('/vialPoseFromTF',SetString, self.setVialPoseViaStr)
        self.track_pose_array = rospy.Service('/vialPoseFromFloatArray',Float64MultiArraySrv, self.setVialPoseViaFlt)

        self.csv_rec_manager = rospy.Service("/manage_csv_recording",SetBool,self.csvRecordTrigger)

        while not rospy.is_shutdown():
            if self.changeTwistRef:
                suc = True
                if not self.asPolar:
                    req = SetStringRequest()
                    if self.isTwistLocal:
                        ## from Local to Global
                        req.data = self.refCubeName
                    else:
                        req.data = "LOCAL"
                    suc,resp,ret = self.cM.call_service("/set_twist_kind",self.change_twist_interface,req)
                    if suc:
                        self.isTwistLocal = not self.isTwistLocal
                else:
                    self.isTwistLocal = False   

                    ## change state according to buttons
                    k = 0
                    while k < len(self.curButtons):
                        if self.curButtons[k] == 1:
                            concernedState = self.butState[k]
                            if concernedState == self.curButtonState:
                                concernedState = "POLAR"
                            self.curButtonState = concernedState
                            k = len(self.curButtons)
                        k+=1
                        

                    req = SetStringRequest()
                    req.data = self.curButtonState
                    #print(self.curButtonState)
                    suc,resp,ret = self.cM.call_service("/set_twist_kind",self.change_twist_interface,req)


                if suc:

                    self.changeTwistRef = False
            ## do not update according to twist 
            ## if twist is near zero
            ## 0.0017 : equivalent to 0.1 deg / s
            if np.linalg.norm(self.twist) > (0.0017/self.nodeFreq):
                
                self.setVialPoseViaPolarTwist()
            # else:
            #     self.twist = np.array([0.0,0.0,0.0,0.0,0.0,0.0])
            if self.pauseCode:
                s = input("Enter to continue")
                self.pauseCode = False


            self.tU.updateSTS()

            self.rate.sleep()

    # def frameCallback(self, msg ):

    #     time = rospy.Time.now()
    #     self.br.sendTransform( (0, 0, sin(self.counter/140.0)*2.0), (0, 0, 0, 1.0), time, self.originFrame, "moving_frame" )
    #     self.counter += 1
    def getSensibility(self, msg):
        self.twistSensibility = msg.data

    def cubeUpdate(self,M,typeCube="long",computeType = "byAngles"):
        """!

        A partir d'une orientation de la TF associée au cube
        (R^{Bk}_{A}), réoriente les différents marqueurs (cube, arrow) et les TF associés
        (donc : R^{Ck}_{A}, R^{Dk}_{A} )
        
        Avec : 
            * typeCube : the type of cube that is updated : 
                - long : Bk : M = R^{Bk}_{A}
                - trans : Ck : M = R^{Ck}_{Bk}
            * 
        
        Poss 1 : (byTF)
        Pour cela : on se ramène à R^{Dk}_{A}, on réapplique cubeUpdate.
        
        Poss 2 : (byAngles)
        On revient à des angles

        NB : bug encore avec byTF (singularités). Faire plutôt avec byAngles.

        """

        ## Poss 1 :
        if computeType == "byTF":
            nameRef = ""
            if typeCube == "long":
                nameRef = self.refCubeLong
                #print("matrix long : ", self.tU.M2P(M))
                suc = False
                L_Bk1_A = []

                ## poss 2 : ac angles

                # while not rospy.is_shutdown() and not suc:
                #     L_Bk1_A,suc = self.tU.getPose(self.refG_marker, self.refCubeLong)

                # M_Bk1_A = self.tU.L2M(L_Bk1_A)
                # betak = np.arctan2(M[1,0],M[0,0])

                # betak1 = np.arctan2(M_Bk1_A[1,0],M_Bk1_A[0,0])

                # self.anglesToMarker([betak-betak1,0.0,0.0])

                ## poss 1 : 

                suc = False
                L_Dk1_ref1 = []
                while not rospy.is_shutdown() and not suc:
                    L_Dk1_ref1,suc = self.tU.getPose(nameRef,self.vialName)

                R_ref1_A = M 
                            
                R_Dk_A = R_ref1_A @ self.tU.L2M(L_Dk1_ref1)
                R_Dk_vialRG = np.linalg.inv(self.M_vialRG_A) @ R_Dk_A

                self.arrowUpdate(R_Dk_vialRG)


                #input()

            else:
                nameRef = self.refCubeTrans
            # print(self.tU.M2P(M))
            # input()

                suc = False
                L_Dk1_ref1 = []
                while not rospy.is_shutdown() and not suc:
                    L_Dk1_ref1,suc = self.tU.getPose(nameRef,self.vialName)

                R_ref1_A = M 
                
                if typeCube == "trans":
                    ## here, M = R_Bk_Ck
                    suc = False
                    L_Bk1_A = []
                    while not rospy.is_shutdown() and not suc:
                        L_Bk1_A,suc = self.tU.getPose(self.refG_marker,self.refCubeLong)

                    R_ref1_A = self.tU.L2M(L_Bk1_A) @ M

                
                R_Dk_A = R_ref1_A @ self.tU.L2M(L_Dk1_ref1)
                R_Dk_vialRG = np.linalg.inv(self.M_vialRG_A) @ R_Dk_A

                self.arrowUpdate(R_Dk_vialRG)

            # ## Poss 2 :
            # Lrpy = self.tU.M2Lrpy(M)

            # ## Faire diff ac R^{Ck-1}_{A} au bsn (pas sûr, si marker ne se met bien pas à jour)

            # self.anglesToMarker(Lrpy)
        elif computeType == "byAngles":
            if typeCube == "long":
                M_Bk_A = M
                suc = False
                L_Bk1_A = []
                while not rospy.is_shutdown() and not suc:
                    L_Bk1_A,suc = self.tU.getPose(self.refG_marker, self.refCubeLong)

                M_Bk1_A = self.tU.L2M(L_Bk1_A)

                M_Bk_Bk1 = np.linalg.inv(M_Bk1_A) @ M_Bk_A

                phi_k = np.arctan2(M_Bk_Bk1[1,0],M_Bk_Bk1[0,0] )

                self.anglesToMarker([phi_k,0.0,0.0])
            
            elif typeCube == "trans":
                M_Ck_Bk = M
                suc = False
                L_Bk1_Ck1 = []                
                while not rospy.is_shutdown() and not suc:
                    L_Bk1_Ck1,suc = self.tU.getPose(self.refCubeTrans, self.refCubeLong)

                M_Bk1_Ck1 =  self.tU.L2M(L_Bk1_Ck1)

                M_Ck_Ckp1 = M_Bk1_Ck1 @ M_Ck_Bk

                tht_k = np.arctan2(M_Ck_Ckp1[0,2],M_Ck_Ckp1[0,0] )
                self.anglesToMarker([0.0,tht_k,0.0])
            




    def arrowUpdate(self,M):
        """!
        A partir d'une orientation de la TF associée à la fiole
        (R^{Dk}_{A}), réoriente les différents marqueurs (cube, arrow) et les TF associés
        (donc : R^{Bk}_{A}, R^{Ck}_{A} )
        
        Avec : 
            * M : R^{Dk}_{vialRG}, la nouvelle orientation de la flèche
            (exprimée dans le repère vialRG)

        Énormes bugs lorsqu'on est proche des singularités... mis de côté pour le moment.
        
        """
        ## first, turn back M to R^{Dk}_{A}
        ## (for all future calculus)
        R_Dk_A = self.M_vialRG_A @ M

        if self.fixed:
            ## pas de composante en translation
            for k in range(3):
                R_Dk_A[k,3] = 0.0

        ## deb : vérifie prés de singularité
        beta_k = 0
        R_Bk_A = np.identity(4)
        R_Ck_Bk = np.identity(4)
        self.inputCheck=False

        singCase = False
        #print(R_Dk_A[2,2])
        if abs( abs(R_Dk_A[2,2]) - 1) < self.thresSing:
            singCase = True
            #self.inputCheck=True
            ## singularity case : sin(beta) = 0
            ## on fixe R^{Bk}_{A} = R^{Bk-1}_{A}, afin d'éviter une
            ## modification involontaire du cercle vert
            # suc = False
            # ## R^{B_{k-1}}_{A}
            # L_Bk1_A = []
            # suc_Bk1_A = False

            # # axis = np.zeros((3,1))
            # # axis[2] = 1.0
            # # self.sign_sinbk = np.sign(np.dot(R_Dk_A[:3,2],axis)) 


            # while not rospy.is_shutdown() and not suc:
            #     L_Bk1_A,suc_Bk1_A = self.tU.getPose(self.refG_marker, self.refCubeLong)

            #     suc = (suc_Bk1_A)
            
            # if suc:

            #     R_Bk_A = self.tU.L2M(L_Bk1_A)

            #     beta_k = np.arccos(R_Dk_A[2,2])
            #     # print(R_Dk_A[2,2])

            #     #print(beta_k)

            #     R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
            #                         [0.0, 1.0, 0.0,0.0],
            #                         [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
            #                         [0.0, 0.0, 0.0, 1.0]                        
            #                         ])
            #     # print(R_Ck_Bk)
            #     # input()

            ## poss 2 : on fait en sorte que R^{Dk}_{Ck} = R^{Dk-1}_{Ck-1}
            ## (e.g on annule toute modification de l'axe de la fiole)
            ## risques d'instabilité à singularité :( 
            suc = False
            ## R^{B_{k-1}}_{A}
            L_Dk1_Ck1 = []
            suc_Dk1_Ck1 = False

            # axis = np.zeros((3,1))
            # axis[2] = 1.0
            # self.sign_sinbk = np.sign(np.dot(R_Dk_A[:3,2],axis)) 


            while not rospy.is_shutdown() and not suc:
                L_Dk1_Ck1,suc_Dk1_Ck1 = self.tU.getPose(self.refCubeTrans, self.vialName)

                suc = (suc_Dk1_Ck1)
            
            if suc:

                ## 1 : obtenir R_Bk_A, avec projeté.
                normal = np.zeros((4,))
                normal[2] = 1.0
                normal[3] = 1.0

                pt = np.zeros((3,))    
            
                R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
                
                R_Dk_A_XY = np.dot(R,R_Dk_A)

                ## position axe Z sur plan X-Y (= pointe de fiole)
                R_Dk_A_XY_axeZ = R_Dk_A_XY[:3,2]

                print("Z sur plan X-Y : ", R_Dk_A_XY_axeZ)
                if np.max([abs(R_Dk_A_XY_axeZ[0]), abs(R_Dk_A_XY_axeZ[1])] ) < self.thresCompute:
                    print("special sing case")
                    ## R_Bk_A = R_Bk1_A
                    suc = False
                    ## R^{B_{k-1}}_{A}
                    L_Bk1_A = []
                    suc_Bk1_A = False

                    # axis = np.zeros((3,1))
                    # axis[2] = 1.0
                    # self.sign_sinbk = np.sign(np.dot(R_Dk_A[:3,2],axis)) 


                    while not rospy.is_shutdown() and not suc:
                        L_Bk1_A,suc_Bk1_A = self.tU.getPose(self.refG_marker, self.refCubeLong)

                        suc = (suc_Bk1_A)
                    
                    if suc:

                        R_Bk_A = self.tU.L2M(L_Bk1_A)

                        alpha_k = np.arctan2( R_Bk_A[1,0], R_Bk_A[0,0]  )

                        beta_k = np.arctan2(R_Dk_A[0,2]/np.cos(alpha_k), R_Dk_A[2,2] )

                        # if np.cos(alpha_k) != 0.0:
                        #     sin_betak = R_Dk_A[0,2]/np.cos(alpha_k)
                        # else:
                        #     sin_betak = R_Dk_A[1,2]/np.sin(alpha_k)
                        # print("sin_betak : ", sin_betak)
                        # beta_k = np.arcsin(sin_betak)
                        sin_betak = np.sin(beta_k)
                        eps_k =  np.arctan2( R_Dk_A[2,1]/sin_betak, -R_Dk_A[2,0]/sin_betak  )
                        R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
                                            [0.0, 1.0, 0.0,0.0],
                                            [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
                                            [0.0, 0.0, 0.0, 1.0]                        
                                            ])

                else:
                    print("not special sing case")
                    prod_scal = np.dot(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
                    prod_vec = np.cross(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
                    
                    print(prod_vec)
                    ## angle
                    ## sinus issu de : http://villemin.gerard.free.fr/aMaths/GeomAnal/SinusPV.htm
                    ## cos issu de : https://fr.wikihow.com/calculer-l%E2%80%99angle-entre-deux-vecteurs
                    cos = prod_scal/np.linalg.norm(R_Dk_A_XY_axeZ)  
                    sin = np.linalg.norm(prod_vec)/np.linalg.norm(R_Dk_A_XY_axeZ) 

                    print("cos : ", cos)
                    print("sin : ", sin) 
                    
                    sign = np.sign(R_Dk_A_XY_axeZ[1] )

                    tht2 = np.arctan2(-sin,cos)
                    tht = np.arctan2(R_Dk_A_XY[1,0],R_Dk_A_XY[0,0])
                    tht3 = np.arctan2(-R_Dk_A_XY[1,2],R_Dk_A_XY[0,2])
                    print("tht (col 1):",tht)
                    print("tht (sin,cos) : ", tht2)
                    print("tht (col 3): ", tht3)
                    
                    alpha_k = tht2
                    # print(alpha_k)

                    R_Bk_A = np.array([[np.cos(alpha_k), -np.sin(alpha_k), 0.0,0.0],
                                        [np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
                                        [0.0, 0.0, 1.0, 0.0],
                                        [0.0, 0.0, 0.0, 1.0]                                                               
                                        ])
            
                    R_Ck_Bk = np.linalg.inv(R_Bk_A) @ R_Dk_A @ np.linalg.inv( self.tU.L2M(L_Dk1_Ck1) )


                # # ## arccos : pas de signe, pbme.
                # # ## faire plutôt projeté. 
                # beta_k = np.arccos(R_Dk_A[2,2])
                # # print(R_Dk_A[2,2])

                # #print(beta_k)

                # R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
                #                     [0.0, 1.0, 0.0,0.0],
                #                     [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
                #                     [0.0, 0.0, 0.0, 1.0]                        
                #                     ])
            
                # R_Bk_A = R_Dk_A @ np.linalg.inv( self.tU.L2M(L_Dk1_Ck1) ) @ np.linalg.inv(R_Ck_Bk)
                
                # # print(R_Ck_Bk)
                # # input()
        
        ## get R_Bk_A

        
        
        else:

            # L_sign_sinBK = []

            # ## test 1 : on essaye les 2 poss, on voit si on arrive ou non à la même chose
            # L_sign_sinBK = [1, -1]
            # ## ==> pas un critère, renvoie les mêmes erreurs quelque soit signe.
            # ## test 2 : via une valeur fixée
            # axis = np.zeros((3,1))
            # axis[2] = 1.0
            # # L_sign_sinBK = [ np.sign(np.dot(R_Dk_A[:3,2],axis)) ]

            # ## poss 3:

            # L_sign_sinBK = [1]
            # # L_sign_sinBK = [-1]          
            # # 
            # # poss 4 :
            # #L_sign_sinBK = [self.sign_sinbk]
            # best_err = 1e6
            # best_alphak = None
            # best_sign = None

            # for l in range(len(L_sign_sinBK)):
            #     sign_sinBK = L_sign_sinBK[l]

            #     bk = np.arctan2( sign_sinBK*np.sqrt( (R_Dk_A[0,2])**2 + (R_Dk_A[1,2])**2 )  ,  R_Dk_A[2,2] )
            #     try:
            #         beta_k = bk[0]
            #     except IndexError:
            #         beta_k = bk

            #     ## correct representation : http://webserver2.tecgraf.puc-rio.br/~mgattass/demo/EulerAnglesRotations-GimballLock/euler.html
            #     ## !! np.arctan2(y,x)
            #     sin_betak = np.sin(beta_k)

            #     alpha_k = np.arctan2( R_Dk_A[1,2]/sin_betak, R_Dk_A[0,2]/sin_betak  )

            #     eps_k =  np.arctan2( R_Dk_A[2,1]/sin_betak, -R_Dk_A[2,0]/sin_betak  )

            #     R_Bk_A = np.array([[np.cos(alpha_k), -np.sin(alpha_k), 0.0,0.0],
            #                         [np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
            #                         [0.0, 0.0, 1.0, 0.0],
            #                         [0.0, 0.0, 0.0, 1.0]                                                               
            #                         ])
                
            #     R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
            #                         [0.0, 1.0, 0.0,0.0],
            #                         [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
            #                         [0.0, 0.0, 0.0, 1.0]                        
            #                         ])
            
            #     R_Dk_Ck = np.array([[np.cos(eps_k), -np.sin(eps_k), 0.0,0.0],
            #                         [np.sin(eps_k), np.cos(eps_k), 0.0, 0.0],
            #                         [0.0, 0.0, 1.0, 0.0],
            #                         [0.0, 0.0, 0.0, 1.0]                                                               
            #                         ])
            #     # print(beta_k)
            #     # print(M)
            #     # print(R_Dk_Ck)

                
            #     matErr = R_Bk_A @ R_Ck_Bk @ R_Dk_Ck @ np.linalg.inv(R_Dk_A)
                
            #     err = np.linalg.norm(matErr - np.identity(4))

            #     # print("For sign %d , err : %3.4f"%(sign_sinBK,err))
            #     # print("result Mat : ", self.tU.M2Lrpy(matErr))
            #     # print(matErr)

            #     if err < best_err:
            #         best_err = err
            #         best_alphak = alpha_k
            #         best_sign = L_sign_sinBK[l]

            # # print("best sign :", best_sign)
            # alpha_k = best_alphak
            # R_Bk_A = np.array([[np.cos(alpha_k), np.sin(alpha_k), 0.0,0.0],
            #                     [-np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
            #                     [0.0, 0.0, 1.0, 0.0],
            #                     [0.0, 0.0, 0.0, 1.0]                                                               
            #                     ])
            ## Poss 5 :
            ## - Obtenir R^{Bk}_{A} à partir du projeté
            ## de R^{Dk}_{A} sur le plan (X-Y)
            ## - Grâce à R^{Bk}_{A}, en déduire alpha_k,
            ## donc sin(beta_k)

            ## instabilité lorsque x_Bk = z_Dk

            print("not sing")
            ## OK EN DEHORS DE SINGULARITÉ, ON NE TOUCHE PLUS!

            normal = np.zeros((4,))
            normal[2] = 1.0
            normal[3] = 1.0

            pt = np.zeros((3,))    
        
            R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
            
            R_Dk_A_XY = np.dot(R,R_Dk_A)

            ## position axe Z sur plan X-Y (= pointe de fiole)
            R_Dk_A_XY_axeZ = R_Dk_A_XY[:3,2]/np.linalg.norm( R_Dk_A_XY[:3,2])
            print("Z sur plan X-Y : ", R_Dk_A_XY_axeZ)
            prod_scal = np.dot(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
            prod_vec = np.cross(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
            
            print(prod_vec)
            ## angle
            ## sinus issu de : http://villemin.gerard.free.fr/aMaths/GeomAnal/SinusPV.htm
            ## cos issu de : https://fr.wikihow.com/calculer-l%E2%80%99angle-entre-deux-vecteurs
            cos = prod_scal/np.linalg.norm(R_Dk_A_XY_axeZ)  
            sin = np.linalg.norm(prod_vec)/np.linalg.norm(R_Dk_A_XY_axeZ) 

            sign = np.sign(R_Dk_A_XY_axeZ[1] )

            #tht2 = np.arctan2(-sin,cos)
            ## ==> OK, STABLE
            tht2 = np.arctan2(sign*sin,cos)
            tht = np.arctan2(R_Dk_A_XY[1,0],R_Dk_A_XY[0,0])
            tht3 = np.arctan2(-R_Dk_A_XY[1,2],R_Dk_A_XY[0,2])
            print("tht (col 1):",tht)
            print("tht2 (sin, cos) : ", tht2)
            print("tht3 (col 3) : ", tht3)

            #     tht = tht2
                
            # print("tht:",tht)
            # print("tht2 : ", tht2)
            # print(R_Dk_A_XY[1,0]," , ", R_Dk_A_XY[0,0])
            
            # print("cos_betak : ", R_Dk_A[2,2])
            # #if R_Dk_A[2,2]< self.thresSing:
            # print("projeté : ",R_Dk_A_XY[:3,:3])


            # print("tht:",tht)
            alpha_k = tht2
            # print(alpha_k)

            R_Bk_A = np.array([[np.cos(alpha_k), -np.sin(alpha_k), 0.0,0.0],
                                [np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
                                [0.0, 0.0, 1.0, 0.0],
                                [0.0, 0.0, 0.0, 1.0]                                                               
                                ])
            
            # abs_sin_bk = np.sqrt( (R_Dk_A[1,2])**2 + (R_Dk_A[0,2])**2)
            # L_sign = [-1,1]
            # err = 1e6
            # sign = None
            # for k in range(len(L_sign)):
            #     sin_bk = abs_sin_bk*L_sign[k]
            #     alpha_k2 = np.arctan2(R_Dk_A[1,2]/sin_bk, R_Dk_A[0,2]/sin_bk)
            #     print("sign : %d ; alphak2 - alpha_k :"%(L_sign[k]), alpha_k2-alpha_k)
            #     if (abs(alpha_k2 - alpha_k) < err):
            #         err = alpha_k2 - alpha_k
            #         sign = L_sign[k]
            # beta_k = np.arcsin(sign*abs_sin_bk)

            beta_k = np.arctan2(R_Dk_A[0,2]/np.cos(alpha_k), R_Dk_A[2,2] )
            print("beta_k : ", beta_k)
            # if np.cos(alpha_k) != 0.0:
            #     sin_betak = R_Dk_A[0,2]/np.cos(alpha_k)
            # else:
            #     sin_betak = R_Dk_A[1,2]/np.sin(alpha_k)
            # print("sin_betak : ", sin_betak)
            # beta_k = np.arcsin(sin_betak)
            sin_betak = np.sin(beta_k)
            eps_k =  np.arctan2( R_Dk_A[2,1]/sin_betak, -R_Dk_A[2,0]/sin_betak  )
            R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
                                [0.0, 1.0, 0.0,0.0],
                                [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
                                [0.0, 0.0, 0.0, 1.0]                        
                                ])
            
            # print("R_Ck_Bk : ", R_Ck_Bk)
        
            R_Dk_Ck = np.array([[np.cos(eps_k), -np.sin(eps_k), 0.0,0.0],
                                [np.sin(eps_k), np.cos(eps_k), 0.0, 0.0],
                                [0.0, 0.0, 1.0, 0.0],
                                [0.0, 0.0, 0.0, 1.0]                                                               
                                ])
            if R_Dk_A[2,2]< self.thresSing:
                print("sing - treatment")
                #input()
        
        # normal = np.zeros((4,))
        # normal[2] = 1.0
        # normal[3] = 1.0

        # pt = np.zeros((3,))    
    
        # R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
        
        # P_XY = np.dot(R,R_Dk_A)
        # print("R_Dk_A sur X-Y : ", P_XY[:3,:3])

        # print("R_Bk_A : ", R_Bk_A[:3,:3])

        R_Dk_vialRG = np.linalg.inv(self.M_vialRG_A) @ R_Dk_A
        R_Ck_A = R_Bk_A @ R_Ck_Bk
        # print(R_Bk_A)
        # print(R_Ck_A)
        # input()
        ## now update everyone

        ## Bk
        self.markerAndRefUpdate(R_Bk_A,R_Ck_A,R_Dk_vialRG)
        # P = self.tU.M2P(R_Bk_A)
        # suc = False
        # while not rospy.is_shutdown() and not suc:
        #     suc = self.server.setPose(self.refCubeName, P)
        # if suc:
        #     suc = self.setPoseInRef(self.refCubeName,self.tU.M2T(R_Bk_A), self.refG_marker)

        #     ## Dk
        #     # MendW = R_Dk_vialRG
        #     MendW = M            
        #     Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
        #     P = self.tU.M2P(Mreal)
        #     suc = False
        #     while not rospy.is_shutdown() and not suc:
        #         suc = self.server.setPose(self.vialName, P)
        #     if suc:
        #         suc = self.setPoseInRef(self.vialName,self.tU.M2T(R_Dk_vialRG), self.vialRG)

        #         ## Ck
        #         suc = self.setPoseInRef(self.refCubeTrans,self.tU.M2T(R_Bk_A), self.refG_marker)






    def anglesToMarker(self,L):
        """!
        
        Stating from angles L = [phi_k, tht_k, gamma_k], returns orientations of all referentials : 

        A : refG_marker
        Bk : refCubeName
        Ck : recCubePos
        Dk : vialName
        
        

        * R^{D}_{A} ==> orientation de marqueur + TF
        * R^{C}_{A} ==> orientation TF
        * R^{B}_{A} ==> orientation de cube + TF

        Avec : 
            * phi_k : angle entre Bk-1 et Bk. (cercle bleu)
            * tht_k : angle entre B' et C (cercle vert)
            * gamma_k : angle entre C' et D. (cercle rouge)
        
        """

        suc = False
        ## R^{B_{k-1}}_{A}
        L_Bk1_A = []
        suc_Bk1_A = False
        ## R^{C_{k-1}}_{B_{k-1}} = R^{C'_{k-1}}_{B_{k}}
        L_Ck1_Bk1 = []
        suc_Ck1_Bk1 = False
        ## R^{D_{k-1}}_{C_{k-1}} = R^{D'_{k-1}}_{C_{k}}
        L_Dk1_Ck1 = []
        suc_Dk1_Ck1 = False
        while not rospy.is_shutdown() and not suc:
            L_Bk1_A,suc_Bk1_A = self.tU.getPose(self.refG_marker, self.refCubeLong)

            L_Ck1_Bk1,suc_Ck1_Bk1 = self.tU.getPose(self.refCubeLong, self.refCubeTrans)

            L_Dk1_Ck1,suc_Dk1_Ck1 = self.tU.getPose(self.refCubeTrans, self.vialName)

            suc = (suc_Bk1_A) and (suc_Ck1_Bk1) and (suc_Dk1_Ck1)
        
        if suc : 
            R_Bk1_A = self.tU.L2M(L_Bk1_A)
            R_Ck1_Bk1 = self.tU.L2M(L_Ck1_Bk1)
            R_Dk1_Ck1 = self.tU.L2M(L_Dk1_Ck1)
            ## check singularity

            R_Dk1_A = np.dot( np.dot(R_Bk1_A, R_Ck1_Bk1), R_Dk1_Ck1)

            if abs( abs(R_Dk1_A[2,2]) - 1) < self.thresSing:
                #print("sing case")
                ## singularity case : sin(beta) = 0
                ## correction de gamma_k tel que toute rotation 
                ## suivant phi_k (= déplacement sur un cercle) ne provoque pas une rotation de la fiole
                ## suivant son axe.
                dotVal = np.dot(R_Dk1_A[:3,2],R_Bk1_A[:3,2])


                if dotVal > 0:
                    ## deb : cas ici
                    L[2] = L[2] - L[0]
                else:
                    L[2] = L[2] + L[0]
                


            ## computes necessary matrix

            ## R^{Bk}_{B_{k-1}} (z)
            R_Bk_Bk1 = np.identity(4)
            ## R^{Ck}_{C'_{k-1}} (y)
            R_Ck_Cpk1 = np.identity(4)
            ## R^{Dk}_{D'_{k-1}} (z)
            R_Dk_Dpk1 = np.identity(4)
            ## correct representation : http://webserver2.tecgraf.puc-rio.br/~mgattass/demo/EulerAnglesRotations-GimballLock/euler.html

            if L[0] != 0.0:
                a = L[0]
                R_Bk_Bk1 = np.array([[np.cos(a), -np.sin(a), 0.0,0.0],
                                    [np.sin(a), np.cos(a), 0.0, 0.0],
                                    [0.0, 0.0, 1.0, 0.0],
                                    [0.0, 0.0, 0.0, 1.0]                                                               
                                    ])
            if L[1] != 0.0:            
                a = L[1]
                R_Ck_Cpk1 = np.array([[np.cos(a), 0.0, np.sin(a),0.0],
                                    [0.0, 1.0, 0.0,0.0],
                                    [-np.sin(a), 0.0, np.cos(a),0.0],    
                                    [0.0, 0.0, 0.0, 1.0]                        
                                    ])

            if L[2] != 0.0:
                a = L[2]
                R_Dk_Dpk1 = np.array([[np.cos(a), -np.sin(a), 0.0,0.0],
                                    [np.sin(a), np.cos(a), 0.0, 0.0],
                                    [0.0, 0.0, 1.0, 0.0],
                                    [0.0, 0.0, 0.0, 1.0]                                                               
                                    ])
            

            R_Bk_A = np.dot(R_Bk1_A, R_Bk_Bk1)
            R_Ck_A = np.dot( np.dot(R_Bk_A, R_Ck1_Bk1 ),  R_Ck_Cpk1 )
            R_Dk_A = np.dot( np.dot(R_Ck_A, R_Dk1_Ck1 ),  R_Dk_Dpk1 )
            ## correction à appliquer ici : parfois, les opérations font que
            ## l'axe Z de R_DK_A et de R_Ck_A ne sont pas confondus.
            ## e.g : R_Dk_Ck n'est pas uniquement une rotation suivant l'axe Z.
            ## ajouter une vérif : si est le cas, corriger R_Dk_A
            ## tel que R_Dk_A = R_Ck_A * rot(gamma)

            R_Dk_Ck = np.linalg.inv(R_Ck_A) @ R_Dk_A
            if (np.max([abs(R_Dk_Ck[0,2]), abs(R_Dk_Ck[1,2])]) > 1e-8):
                # print(R_Dk_Ck)
                ## detection of smthg wrong
                ## TODO : ajouter meilleure correction.
                for i in range(2):
                    R_Dk_Ck[i,2] = 0.0
                    R_Dk_Ck[2,i] = 0.0
                    R_Dk_A = R_Ck_A @ R_Dk_Ck




            R_Dk_vialRG = np.linalg.inv(self.M_vialRG_A) @ R_Dk_A

            ## now update everyone
            self.markerAndRefUpdate(R_Bk_A, R_Ck_A, R_Dk_vialRG)

    def markerAndRefUpdate(self,R_Bk_A,R_Ck_A,R_Dk_vialRG):
        """!
        
        Given matrix of all elements in their respective referential,
        change marker display and TF. 
        
        """


        ## Bk
        P = self.tU.M2P(R_Bk_A)
        suc = False
        while not rospy.is_shutdown() and not suc:
            suc = self.server.setPose(self.refCubeLong, P)
        if suc:

            suc = self.setPoseInRef(self.refCubeLong,self.tU.M2T(R_Bk_A), self.refG_marker)

            ## Dk
            MendW = R_Dk_vialRG
            Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
            P = self.tU.M2P(Mreal)
            suc = False
            while not rospy.is_shutdown() and not suc:
                #print(P)
                suc = self.server.setPose(self.vialName, P)
            if suc:
                suc = self.setPoseInRef(self.vialName,self.tU.M2T(R_Dk_vialRG), self.vialRG)

                ## Ck
                R_Ck_Bk = np.linalg.inv(R_Bk_A) @ R_Ck_A
                # print(self.tU.M2P(R_Ck_Bk))
                suc = self.setPoseInRef(self.refCubeTrans,self.tU.M2T(R_Ck_Bk), self.refCubeLong)
                suc = self.server.setPose(self.refCubeTrans, self.tU.M2P(R_Ck_Bk))
        
                ## finally : tfC_refG (according to R^{Dk}_{Ck})
                R_Dk_A = self.M_vialRG_A @ R_Dk_vialRG
                R_Dk_Ck = np.linalg.inv(R_Ck_A) @ R_Dk_A
                R_tfC_A = self.M_tfC_refG @ R_Dk_Ck

                suc = self.setPoseInRef(self.tFCorrName,self.tU.M2T(R_tfC_A),self.refG_marker)


        if self.inputCheck:
            ## For debugging
            input()
                
    def setVialPoseViaPolarTwist(self):
        """!
        Better, previous function was too messy
        
        
        """
        twist_used = copy.deepcopy(self.twist)
        #self.twistDebug = True


        ## x
        isv = 0
        ## y
        #isv = 1
        ## z
        #isv = 2  



        if self.twistDebug:                      
        
            for i in range(5):
                if (i != isv):
                    twist_used[i] = 0.0
                else:
                    sign = 1
                    if self.asPolar:
                        sign = np.sign(twist_used[i])
                    twist_used[i] = sign* self.cMax * self.twistSensibility 
        
        if self.asPolar:
            iChosen = -1
            #print(twist_used)

            if twist_used[2] != 0:
                ## Z-axis of vial case

                iChosen = 2

            elif twist_used[1] != 0:
                ## left-right
                if self.isSim:
                    twist_used[2] = twist_used[1]
                else:
                    twist_used[2] = twist_used[1]
                twist_used[1] = 0
                iChosen = 1

            else:
                ## upDown
                ## classic 3d mouse : 
                ## pressToDown (hand on up side, go to down) ==> positive ==> should go down
                ## pressToUp (hand on down side, go to up) ==> negative ==> should go up
                if self.isSim:
                    twist_used[1] = -twist_used[0]
                else:
                    twist_used[1] = -twist_used[0]                    
                twist_used[0] = 0
                iChosen = 0

            M_dt = mr.MatrixExp6( mr.VecTose3(twist_used) )
            if iChosen ==2:
                ## zAxis
                if self.inRecording:
                   
                    s = self.curButtonState.replace("_","")
                    curState = "3dMouse_%s_AR"%(s)
                    if self.curMode != curState:
                        self.curMode = curState                           
                        self.modeUsed.append(self.curMode)
                        self.begTimeMode.append(rospy.Time.now().to_sec() )


                L,suc = self.tU.getPose(self.vialRG,self.vialName)
                if suc:
                    M = np.dot( self.tU.L2M(L) ,M_dt)

                    for i in range(3):
                        M[i,3] = self.posOri[i]
                    
                    self.setPoseForAll(M)
                    
            
            elif iChosen == 1:
                ## left-right
                if self.inRecording:
                    s = self.curButtonState.replace("_","")                    
                    curState = "3dMouse_%s_LR"%(s)
                    if self.curMode != curState:
                        self.curMode = curState                           
                        self.modeUsed.append(self.curMode)
                        self.begTimeMode.append(rospy.Time.now().to_sec() )


                L,suc = self.tU.getPose(self.refG_marker,self.refCubeTrans)
                if suc:
                    M = np.dot( self.tU.L2M(L) ,M_dt)
                    self.leftRightMvt(M)

                    self.updateTht()
                
            elif iChosen == 0:
                ## upDown
                if self.inRecording:
                    s = self.curButtonState.replace("_","")                    
                    curState = "3dMouse_%s_UD"%(s)
                    if self.curMode != curState:
                        self.curMode = curState                           
                        self.modeUsed.append(self.curMode)
                        self.begTimeMode.append(rospy.Time.now().to_sec() )

                L,suc = self.tU.getPose(self.refG_marker,self.refCubeTrans)

                # L2,suc = self.tU.getPose(self.refCubeTrans, self.recordedCubePos)
                if suc:
                    ## new pose
                    # M = np.dot( self.tU.L2M(L2) ,M_dt)

                    #print(self.tU.M2Lrpy(M))
                    ## reexpress in good ref
                    ## ==> provoque gd mvts, pbme
                    # M2 = np.dot( self.tU.L2M(L) , M)

                    ## ok
                    M2 =  np.dot(self.tU.L2M(L), M_dt)

                    self.upDownMvt(M2)
                    self.needToChangeRTNM = True

    def setPoseForAll(self,M):
        """!

        Set pose for vial, in server AND in /tf.
        
        M should be expressed in midVial
        Reset vial pose for TF and server, at the same time
        
        """
        ## Pori : for /TF
        Pori = self.tU.M2P(M)

        ## P : for server

        MendW = M
        Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
        P = self.tU.M2P(Mreal)

        #exit()
        suc = self.server.setPose(self.vialName, P)
        if suc:
            suc = self.setPoseInRef(self.vialName,self.tU.M2T(M), self.vialRG)
            #suc = self.setVialPose(Pori)   
        return(suc)          

    ## def processRGFeedback
        ## contrôle Y-Z d'un objet 3D fixe, invisible (caché au milieu de la fiole, dans lequel est exprimé la fiole),
        ## retransmis ensuite à l'orientation de la fiole
        ##      * Rot sur Z-fixe (left-right): 
        ##         - Rot de axe Z de ref fixe
        ##         - Application de rot sur vial (auto, ok)
        ##              ==> ref fixe a changé d'orientation
        ##      * Rot sur Y-fixe (up-down):
        ##          - Rot de axe Y de ref fixe
        ##          - Application de rot sur vial 
        ##          - Retour de ref fixe à orientation init
        ##              ==> ref fixe ne change pas d'orientation



    def processFeedback(self, feedback ):
        """!
        
        Corriger rotation de cube 
        
        """
        verbose = False
        s = "Feedback from marker '" + feedback.marker_name
        s += "' / control '" + feedback.control_name + "'"

        mp = ""
        if feedback.mouse_point_valid:
            mp = " at " + str(feedback.mouse_point.x)
            mp += ", " + str(feedback.mouse_point.y)
            mp += ", " + str(feedback.mouse_point.z)
            mp += " in frame " + feedback.header.frame_id

        if feedback.event_type == InteractiveMarkerFeedback.BUTTON_CLICK and verbose :
            rospy.loginfo( s + ": button click" + mp + "." )
        elif feedback.event_type == InteractiveMarkerFeedback.MENU_SELECT and verbose:
            rospy.loginfo( s + ": menu item " + str(feedback.menu_entry_id) + " clicked" + mp + "." )
        
        
        elif feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
            #print("Recompute tht because of pose update")
            L = self.tU.P2L(feedback.pose)
            M = self.tU.L2M(L)
            ## reset twist to zero
            self.twist = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
            if feedback.marker_name == self.vialName:
                ### 3D modification or rotation around z axis

                suc = False

                if feedback.control_name != "rotate_z":
                    self.recomputeTht = True
                    if self.inRecording:
                        if self.curMode != "arrow_3d":
                            self.curMode = "arrow_3d"                            
                            self.modeUsed.append(self.curMode)
                            self.begTimeMode.append(rospy.Time.now().to_sec() )

                    ## change L : considers L as expressed in world
                    ## not a good idea AT ALL

                    ## only solution is to reexpressed marker in world
                    # L2, suc = self.tU.getPose(self.refG_marker, self.originFrame)

                    # if suc:
                    #     M_rG_ori = self.tU.L2M(L2)

                    #     L = self.tU.M2L( np.dot(  M_rG_ori  ,self.tU.L2M(L) )   )
                    self.arrowUpdate(self.applyServerToTF(M))
                else:
                    if self.inRecording:
                        if self.curMode != "circle_AR":
                            self.curMode = "circle_AR"                            
                            self.modeUsed.append(self.curMode)
                            self.begTimeMode.append(rospy.Time.now().to_sec() )

                    self.zAxisRot(self.applyServerToTF(M))
                #self.arrowUpdate(M)

                
            elif feedback.marker_name == self.refCubeLong:
                ## 

                if feedback.control_name == "rotate_x_fixed":
                    if self.inRecording:
                        if self.curMode != "circle_LR":
                            self.curMode = "circle_LR"                            
                            self.modeUsed.append(self.curMode)
                            self.begTimeMode.append(rospy.Time.now().to_sec() )

                    ## appliquer rot à Mdes_0_frame
                    self.cubeUpdate(M,"long")
                    #self.leftRightMvt(self.tU.L2M(L))

            elif feedback.marker_name == self.refCubeTrans:
                if feedback.control_name == "rotate_y_fixed":
                    if self.inRecording:
                        if self.curMode != "circle_UD":
                            self.curMode = "circle_UD"                            
                            self.modeUsed.append(self.curMode)
                            self.begTimeMode.append(rospy.Time.now().to_sec() )

                    self.cubeUpdate(M,"trans")
                    #self.upDownMvt(self.tU.L2M(L))

    # TODO
    #          << "\nposition = "
    #          << feedback.pose.position.x
    #          << ", " << feedback.pose.position.y
    #          << ", " << feedback.pose.position.z
    #          << "\norientation = "
    #          << feedback.pose.orientation.w
    #          << ", " << feedback.pose.orientation.x
    #          << ", " << feedback.pose.orientation.y
    #          << ", " << feedback.pose.orientation.z
    #          << "\nframe: " << feedback.header.frame_id
    #          << " time: " << feedback.header.stamp.sec << "sec, "
    #          << feedback.header.stamp.nsec << " nsec" )
        elif feedback.event_type == InteractiveMarkerFeedback.MOUSE_DOWN and verbose :
            rospy.loginfo( s + ": mouse down" + mp + "." )
        elif feedback.event_type == InteractiveMarkerFeedback.MOUSE_UP :
            if feedback.control_name == "rotate_y_fixed":
                ## then, we can update 
                self.needToChangeRTNM = True
            if verbose:
                rospy.loginfo( s + ": mouse up" + mp + "." )
        #print("====")
        
        self.server.applyChanges()



    def alignMarker(self, feedback ):
        pose = feedback.pose


        pose.position.x = round(pose.position.x-0.5)+0.5
        pose.position.y = round(pose.position.y-0.5)+0.5

        rospy.loginfo( feedback.marker_name + ": aligning position = " + str(feedback.pose.position.x) + "," + str(feedback.pose.position.y) + "," + str(feedback.pose.position.z) + " to " +
                                                                        str(pose.position.x) + "," + str(pose.position.y) + "," + str(pose.position.z) )

        self.server.setPose( feedback.marker_name, pose )
        self.server.applyChanges()


    def leftRightMvt(self, M):
        """!
        
        M :  pose de refCubeNameMoving dans refG_marker   
        
        """

        L2,suc = self.tU.getPose(self.refCubeTrans,self.vialName)
        L4, suc = self.tU.getPose(self.vialRG, self.refG_marker  )

        if suc:
            ## ok


            M_markRG_vialRG = self.tU.L2M(L4)
            ## ==> refCubeNameMoving ds refG_marker
            L = self.tU.M2L(M)
            
            newMdes_0_frame = M

            newM = np.dot( M_markRG_vialRG , np.dot( newMdes_0_frame,self.tU.L2M(L2)))
            ## rot ds refG
            self.setPoseInRef(self.refCubeTrans,self.tU.L2T(L), self.refG_marker)

            self.setPoseInRef(self.refCubeName,self.tU.L2T(L), self.refG_marker)

            # self.updateVialPoseInRef = True


            for i in range(3):
                newM[i,3] = self.posOri[i]
            Pori = self.tU.M2P(newM)

            ## P : for server

            MendW = newM
            Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
            P = self.tU.M2P(Mreal)

            #exit()
            suc = self.server.setPose(self.vialName, P)
            if suc:
                suc = self.setPoseInRef(self.vialName,self.tU.P2T(Pori), self.vialRG)  
            
    def zAxisRot(self,M):
        """!
        
        M : pose of vialName in vialRG
        
        """
        R_Dk_vialRG = M
        MendW = R_Dk_vialRG
        Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
        P = self.tU.M2P(Mreal)
        suc = False
        while not rospy.is_shutdown() and not suc:
            #print(P)
            suc = self.server.setPose(self.vialName, P)
        if suc:
            suc = self.setPoseInRef(self.vialName,self.tU.M2T(R_Dk_vialRG), self.vialRG)



    def upDownMvt(self,M):
        """!
        
        M :  pose de refCubeNameMoving dans refG_marker   
        feedback correspond à diff entre pose au moment où tu as commencé mvt de
        cercle - moment actuel
        
        """        
        suc = False
        if self.needToChangeRTNM:
            ### cela permet d'enregistrer l'orientation du référentiel au début du contrôle du marqueur. (vu qu'est une différence par rapport
            ## à la position du cercle au moment où celui-ci est à l'horizontal.)
            ## (en simplifié : peut normalement ne pas être considéré, si on utilise les référentiels : 
            ##      * Le réf B est défini par rapport au ref A, et correspond en permanence à l'orientation du cube.
            ##      * Le réf C est défini par rapport au ref B. La différence du cercle correspond à une quantité entre les réfs B et C. 
            L3,suc = self.tU.getPose(self.refCubeName, self.recordedCubePos)
            if suc :
                self.savedRTNM = self.tU.L2M(L3)
                self.needToChangeRTNM = False

                #print("~~~~~~~")

        else:
            suc = True

        if suc:
            ## feedback correspond à diff entre pose au moment où tu as commencé mvt de
            ## cercle - moment actuel
            L_reset,suc = self.tU.getPose(self.refG_marker,self.refCubeName)
            L2,suc = self.tU.getPose(self.recordedCubePos,self.vialName)
            # L5,suc= self.tU.getPose(self.refG_marker, self.recordedCubePos)
            L4, suc = self.tU.getPose(self.vialRG, self.refG_marker  )

            #L_transfer,suc = self.tU.getPose(self.b)

            if suc:
                ## ==> refCubeNameMoving ds refG_marker (a bougé)
                rTNM_rG = M

                # print("rtnm_rg : ", self.tU.M2Lrpy(rTNM_rG))
                # print("savedRTNM_rG : ", self.tU.M2Lrpy(self.savedRTNM))

                # ## refCubeName ds refG_marker (n'a pas bougé)
                rTN_rG = self.tU.L2M(L_reset)

                # rcP_rG = self.tU.L2M(L5)



                # ## vial ds RTN
                # v_RTN = self.tU.L2M(L2)

                # ## vial ds rG
                # v_RG = np.dot( rTN_rG , v_RTN)

                # ## RTNM ds vial
                # rTNM_V = np.dot( np.linalg.inv(v_RTN),  np.dot( np.linalg.inv( rTN_rG ), rTNM_rG    )   )    

                # ## nV ds V
                # ## pbme : ac ça, nV_V est cst. Or, ce n'est pas ce qu'on veut.
                # nV_V = np.dot(rTNM_V, v_RTN  )


                # #print(self.tU.M2Lrpy(nV_V))

                # ## nV ds rG = newM
                # newM = np.dot( v_RG ,nV_V)

                # ## save pose of rtnM
                # ## savedRTNM is expressed in RTN
                # self.savedRTNM = np.dot( self.tU.L2M(L4), np.dot(  np.linalg.inv(rTN_rG)   ,rTNM_rG ) )

                # #suc = self.setPoseInRef( self.recordedCubePos, self.tU.M2T(savedRTNM), self.refCubeTrans  )
                # # M_displ = np.dot( np.linalg.inv(rTNM_rG), rTN_rG )

                # # print(self.tU.M2Lrpy(M_displ))

                # # L_rpy = self.tU.M2Lrpy(M_displ)
                # # for i in range(3,6):
                # #     if i!= 4:
                # #         L_rpy[i] = 0.0
                # # M_displ = self.tU.Lrpy2M(L_rpy)

                # # newM = np.dot( self.tU.L2M(L2), np.linalg.inv(M_displ) )

                # ## controle en pos
                # #newM = np.dot( rTN_rG, savedRTNM)

                # ## ==> reste en place, logique
                # #newM = np.dot( rTNM_rG, rTNM_V) 

                ### RTNM in RG
                #newM = np.dot(  np.linalg.inv(rTN_rG) , np.dot(self.savedRTNM, rTNM_rG) )

                # newM = np.dot( rTN_rG ,np.dot( self.savedRTNM  ,np.dot( np.linalg.inv(rcP_rG) ,rTNM_rG ) ) )

                
                #newM = rTNM_rG
                rTNM_RTN = np.dot( np.linalg.inv(rTN_rG), rTNM_rG  )

                #print("diff rtnm / rtn : ", self.tU.M2Lrpy(rTNM_RTN))

                ## get RTNM in RTN
                #rTNM_RTN = np.dot( np.linalg.inv(rTN_rG), newM  )
                #rTNM_RTN = np.dot( np.linalg.inv(rTN_rG), rTNM_rG  )

                ## mat2put = orientation de markerCube sauvegardé
                mat2put =np.dot( self.savedRTNM ,rTNM_RTN   )

                
                newM = np.dot(rTN_rG, mat2put   )

                newM = np.dot(newM, self.tU.L2M(L2))

                newM = np.dot( self.tU.L2M(L4)  , newM)

                #newM = np.dot( )
                #rTNM_RTN =  np.dot( np.linalg.inv(rTN_rG), newM  ) 
                #print("new rtnm_rtn saved : ", self.tU.M2Lrpy(mat2put))

                suc = self.setPoseInRef( self.recordedCubePos, self.tU.M2T(mat2put), self.refCubeName  )

                for i in range(3):
                    newM[i,3] = self.posOri[i]
                Pori = self.tU.M2P(newM)

                ## P : for server

                MendW = newM
                Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
                P = self.tU.M2P(Mreal)

                #exit()
                suc = self.server.setPose(self.vialName, P)
                if suc:
                    suc = self.setPoseInRef(self.vialName,self.tU.P2T(Pori), self.vialRG)  
                                        

                ## reset marker at origin pose (to prevent any displacement of the axis)
                suc = self.server.setPose(self.refCubeTrans, self.tU.L2P(L_reset))
                
                if suc:
                    self.setPoseInRef(self.refCubeTrans,self.tU.L2T(L_reset), self.refG_marker)



    def saveMarker(self, int_marker ):
        self.server.insert(int_marker, self.processFeedback)

    def makeVialMarker(self,position):
        """!
        
        2 modes : 
            * Free, orientation only
            * By axes

        """

        ## create TF for refG_marker

        msg =  geometry_msgs.msg.TransformStamped()
        msg.header.frame_id = self.originFrame
        msg.child_frame_id = self.refG_marker
        msg.transform = self.tU.M2T(np.identity(4))
        msg.transform.translation.x = position.x
        msg.transform.translation.y = position.y
        msg.transform.translation.z = position.z

        self.tU.tfToStaticTS(msg)   


        vial_marker = InteractiveMarker()
        vial_marker.header.frame_id = self.vialRG ##self.refG_marker
        vial_marker.pose.position.x = 0.0
        vial_marker.pose.position.y = 0.0
        vial_marker.pose.position.z = 0.0
        vial_marker.pose.orientation.w = 1
        vial_marker.pose.orientation.x = 0
        vial_marker.pose.orientation.y = 0
        vial_marker.pose.orientation.z = 0
        vial_marker.pose.orientation = normalizeQuaternion(vial_marker.pose.orientation)

        # if self.vialInWorld:
        #     vial_marker.header.frame_id = self.originFrame
        #     vial_marker.pose.position = position
        #     vial_marker.pose.orientation.w = 1
        #     vial_marker.pose.orientation.x = 0
        #     vial_marker.pose.orientation.y = 0
        #     vial_marker.pose.orientation.z = 0
        #     vial_marker.pose.orientation = normalizeQuaternion(vial_marker.pose.orientation)

        # else:
        #     vial_marker.header.frame_id = self.vialRG ##self.refG_marker

        #     vial_marker.pose = self.vialMarkerPoseInRG

        vial_marker.scale = 1
        vial_marker.name = self.vialName
        vial_marker.description = "Simple 3-DOF Vial Control"

        ## Cube longitudinal
        cube_long = InteractiveMarker()
        cube_long.header.frame_id = self.refG_marker
        cube_long.pose =  self.tU.M2P(np.identity(4)) ##self.tU.M2P(self.M_oriFixed_oriTwist)
        cube_long.pose.orientation = normalizeQuaternion(cube_long.pose.orientation)

        cube_long.scale = 0.5
        cube_long.name = self.refCubeLong
        cube_long.description = ""

        ## Cube transversal
        cube_trans = InteractiveMarker()
        cube_trans.header.frame_id = self.refCubeLong
        cube_trans.pose = self.tU.M2P(np.identity(4))
        cube_trans.pose.orientation = normalizeQuaternion(cube_trans.pose.orientation)

        cube_trans.scale = 0.5
        cube_trans.name = self.refCubeTrans
        cube_trans.description = ""



        msg = geometry_msgs.msg.TransformStamped()

        # msg.transform.rotation.w = 1
        # msg.transform.rotation.x = 0
        # msg.transform.rotation.y = 0
        # msg.transform.rotation.z = 0

        msg.child_frame_id = vial_marker.name
        msg.header.frame_id = self.vialRG
        msg.header.stamp = rospy.Time.now()

        # rospy.loginfo( s + ": pose changed")

        # rospy.loginfo("[x,y,z] : [%3.4f, %3.4f, %3.4f]"%(feedback.pose.position.x, feedback.pose.position.y, feedback.pose.position.z))
        # rospy.loginfo("[w,x,y,z] : [%3.4f, %3.4f, %3.4f, %3.4f]"%(feedback.pose.orientation.w, feedback.pose.orientation.x, feedback.pose.orientation.y, feedback.pose.orientation.z))


        scale = 10.0
        makeBoxControl(vial_marker, scale, self.useRealVial)
        
        makeBoxControl(cube_long,cube_long.scale,False,False)
        makeBoxControl(cube_trans,cube_trans.scale,False,False)

        cube_long.controls[0].markers[0].color.a = 0.0 ## 1.0 ## to appear
        cube_trans.controls[0].markers[0].color.a = 0.0 ## 1.0 ## to appear

        if self.useRealVial:
            ## use center of vial instead of its base
            ## vial_server => vial_TF
            L = [0.03*scale, 0.0,0.0,0.0,1.57078,0.0]
            #L = [0.03*scale, 0.0,0.0,0.0,0.0,0.0]            
            self.serv2TFTransf = self.tU.Lrpy2M(L)

            Mori = self.tU.P2M(vial_marker.pose)

            #Mnew = self.serv2TFTransf@Mori
            Mnew = Mori@self.serv2TFTransf
            msg.transform = self.tU.M2T(Mnew)

            self.fixed = True

            ## centré sur frame, mais plus centré sur milieu vial :(
            #L[0] = 0.0
            #self.serv2TFTransf = self.tU.Lrpy2M(L)


        else:

            msg.transform = self.tU.P2T(vial_marker.pose)

        self.tU.tfToStaticTS(msg)

        msg.transform = self.tU.P2T(cube_long.pose)

        msg.child_frame_id = self.refCubeLong
        msg.header.frame_id = self.refG_marker
        msg.header.stamp = rospy.Time.now()
        self.tU.tfToStaticTS(msg)

        msg.transform = self.tU.P2T(cube_trans.pose)

        msg.child_frame_id = cube_trans.name
        msg.header.frame_id = cube_long.name
        msg.header.stamp = rospy.Time.now()
        self.tU.tfToStaticTS(msg)

        ## set-up possible interactions with 3D mouse

        ## à essayer : 
        ## contrôle z local
        ## contrôle Y-Z d'un objet 3D fixe, invisible (caché au milieu de la fiole, dans lequel est exprimé la fiole),
        ## retransmis ensuite à l'orientation de la fiole
        ##      * Rot sur Z-fixe (left-right): 
        ##         - Rot de axe Z de ref fixe
        ##         - Application de rot sur vial
        ##              ==> ref fixe a changé d'orientation
        ##      * Rot sur Y-fixe (up-down):
        ##          - Rot de axe Y de ref fixe
        ##          - Application de rot sur vial 
        ##          - Retour de ref fixe à orientation init
        ##              ==> ref fixe ne change pas d'orientation

        ## contrôle 3D libre
        vial_marker.controls[0].interaction_mode = InteractiveMarkerControl.ROTATE_3D 

        ## contrôle de chaque axe local de la vial
        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 1
        control.orientation.y = 0
        control.orientation.z = 0
        control.orientation = normalizeQuaternion(control.orientation)
        control.name = "rotate_z"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS  
        ## test
        ##control.always_visible = False      

        vial_marker.controls.append(control)

        # control = InteractiveMarkerControl()
        # control.orientation.w = 1
        # control.orientation.x = 0
        # control.orientation.y = 1
        # control.orientation.z = 0
        # control.orientation = normalizeQuaternion(control.orientation)
        # control.name = "rotate_x"
        # control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        # vial_marker.controls.append(control)

        # control = InteractiveMarkerControl()
        # control.orientation.w = 1
        # control.orientation.x = 0
        # control.orientation.y = 0
        # control.orientation.z = 1
        # control.orientation = normalizeQuaternion(control.orientation)
        # control.name = "rotate_y"
        # control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        # vial_marker.controls.append(control)
        # control = InteractiveMarkerControl()
        # control.interaction_mode = InteractiveMarkerControl.MENU
        # control.description="Options"
        # control.name = "menu_only_control"
        # # vial_marker.controls.append(copy.deepcopy(control))


        # marker = makeVial( vial_marker )
        # control.markers.append( marker )
        # control.always_visible = True
        # vial_marker.controls.append(control)

        ## contrôle de chaque axe global de la vial
        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 1
        control.orientation.z = 0
        control.orientation = normalizeQuaternion(control.orientation)
        control.name = "rotate_x_fixed"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        cube_long.controls.append(control)

        control = InteractiveMarkerControl()
        control.orientation.w = 1
        control.orientation.x = 0
        control.orientation.y = 0
        control.orientation.z = 1
        control.orientation = normalizeQuaternion(control.orientation)
        ## green circle
        control.name = "rotate_y_fixed"
        control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
        cube_trans.controls.append(control)

        # control.interaction_mode = InteractiveMarkerControl.MENU
        # control.description="Options"
        # control.name = "menu_only_control"
        # vial_marker.controls.append(copy.deepcopy(control))

        self.server.insert(cube_long, self.processFeedback)
        self.menu_handler.apply( self.server, cube_long.name )        

        self.server.insert(cube_trans, self.processFeedback)
        self.menu_handler.apply( self.server, cube_trans.name )        


        self.server.insert(vial_marker, self.processFeedback)
        self.menu_handler.apply( self.server, vial_marker.name )


    def make6DofMarker(self, fixed, interaction_mode, position, show_6dof = False):
        int_marker = InteractiveMarker()
        int_marker.header.frame_id = self.originFrame
        int_marker.pose.position = position
        int_marker.scale = 1

        if self.withVial:

            int_marker.name = "vial"
            int_marker.description = "Simple 3-DOF Vial Control"

        else:

            int_marker.name = "simple_6dof"
            int_marker.description = "Simple 6-DOF Control"

        # insert a box
        makeBoxControl(int_marker, self.withVial)
        int_marker.controls[0].interaction_mode = interaction_mode

        if fixed:
            int_marker.name += "_fixed"
            int_marker.description += "\n(fixed orientation)"

        if interaction_mode != InteractiveMarkerControl.NONE:
            control_modes_dict = { 
                            InteractiveMarkerControl.MOVE_3D : "MOVE_3D",
                            InteractiveMarkerControl.ROTATE_3D : "ROTATE_3D",
                            InteractiveMarkerControl.MOVE_ROTATE_3D : "MOVE_ROTATE_3D" }
            int_marker.name += "_" + control_modes_dict[interaction_mode]
            int_marker.description = "3D Control"
            if show_6dof: 
                int_marker.description += " + 6-DOF controls"
                int_marker.description += "\n" + control_modes_dict[interaction_mode]
            
        if show_6dof: 
            control = InteractiveMarkerControl()
            control.orientation.w = 1
            control.orientation.x = 1
            control.orientation.y = 0
            control.orientation.z = 0
            normalizeQuaternion(control.orientation)
            control.name = "rotate_x"
            control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
            if fixed:
                control.orientation_mode = InteractiveMarkerControl.FIXED
            int_marker.controls.append(control)

            control = InteractiveMarkerControl()
            control.orientation.w = 1
            control.orientation.x = 1
            control.orientation.y = 0
            control.orientation.z = 0
            normalizeQuaternion(control.orientation)
            control.name = "move_x"
            control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
            if fixed:
                control.orientation_mode = InteractiveMarkerControl.FIXED
            int_marker.controls.append(control)

            control = InteractiveMarkerControl()
            control.orientation.w = 1
            control.orientation.x = 0
            control.orientation.y = 1
            control.orientation.z = 0
            normalizeQuaternion(control.orientation)
            control.name = "rotate_z"
            control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
            if fixed:
                control.orientation_mode = InteractiveMarkerControl.FIXED
            int_marker.controls.append(control)

            control = InteractiveMarkerControl()
            control.orientation.w = 1
            control.orientation.x = 0
            control.orientation.y = 1
            control.orientation.z = 0
            normalizeQuaternion(control.orientation)
            control.name = "move_z"
            control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
            if fixed:
                control.orientation_mode = InteractiveMarkerControl.FIXED
            int_marker.controls.append(control)

            control = InteractiveMarkerControl()
            control.orientation.w = 1
            control.orientation.x = 0
            control.orientation.y = 0
            control.orientation.z = 1
            normalizeQuaternion(control.orientation)
            control.name = "rotate_y"
            control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
            if fixed:
                control.orientation_mode = InteractiveMarkerControl.FIXED
            int_marker.controls.append(control)

            control = InteractiveMarkerControl()
            control.orientation.w = 1
            control.orientation.x = 0
            control.orientation.y = 0
            control.orientation.z = 1
            normalizeQuaternion(control.orientation)
            control.name = "move_y"
            control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
            if fixed:
                control.orientation_mode = InteractiveMarkerControl.FIXED
            int_marker.controls.append(control)

        self.server.insert(int_marker, self.processFeedback)
        self.menu_handler.apply( self.server, int_marker.name )

## -----------------End of code--------------------------
      
            ## STRAT 1

            # for i in range(3):
            #     #newM[i,3] = M[i,3]
            #     M[i,3] = self.posOri[i]
            # #print(self.originFrame)
            # #print(self.vialName)
            # #print("current M : ", M)
            # twistInOri = np.dot(mr.Adjoint(M) , self.twist)

            # ## marche pas : 
            # #twistInOri = mr.Adjoint(M) @ self.twist

            # # print("adjoint : ", mr.Adjoint(M))

            # for i in range(3,6):
            #     twistInOri[i] = 0.0

            # print("twist in Ori : ", twistInOri)
            # ## poss 1
            # se3Vel = mr.VecTose3(twistInOri)

            # se3Vel = mr.MatrixExp6(se3Vel)

            # ## poss 2
            # # L2 = [0,0,0,
            # #      twistInOri[0], twistInOri[1], twistInOri[2]]
            # # se3Vel = self.tU.Lrpy2M(L2)

            # #print("twist se3 : ", se3Vel)

            # ## from https://github.com/naoki-mizuno/spacenav_rviz/blob/master/nodes/spacenav_tf.py
            # newM = np.dot(M  , se3Vel)

            # #newM[3,3] = 1.0

            # ## reset newM such as it stays at the same position
            # for i in range(3):
            #     #newM[i,3] = M[i,3]
            #     newM[i,3] = self.posOri[i]
                

            #print("newM : ", newM)

            ## reput in TF ref
            #newM = self.serv2TFTransf @ newM
            # exit()

            ## STRAT 2 (working in local)
                
            # M_dt = mr.MatrixExp6( mr.VecTose3(self.twist) )
            # newM = np.dot(M, M_dt)

            # for i in range(3):
            #     newM[i,3] = self.posOri[i]

            ## STRAT 3 (ko)
            ## Exprimer twist de ref G à local
            # twistFromRefToLocal = self.twist

            # if not self.isTwistLocal:
            #     #print(L2)
            #     M2 = self.tU.L2M(L2)
            #     twistFromRefToLocal = np.dot(mr.Adjoint(M2) , self.twist)
            #     # for i in range(3,6):
            #     #     twistFromRefToLocal[i] = 0.0
            #     ## twist reste bien le même, MAIS M2 change
            #     #print(self.twist)
            #     print(twistFromRefToLocal)

            # ## Exprimer via M_dt
            # M_dt = mr.MatrixExp6( mr.VecTose3(twistFromRefToLocal) )
            # ## test
            # for i in range(3):
            #     M_dt[i,3] = 0.0         
            # newM = np.dot(M, M_dt)
 
            # for i in range(3):
            #     newM[i,3] = self.posOri[i]



                # ### TODO : 
                # ## REVOIR SI MATRICES BIEN DÉF COMME ÉTANT SUR UN AXE
                # ## VOIR COMMENT UTILISER CE TRUC POUR APPLIQUER TWIST PLUTÔT
                # ## VOIR SINON EN DIVISANT : DIMENSION DE TWIST PAR DIMENSION DE TWIST. 
                # ## TWIST SELON X ==> M1
                # ## TWIST SELON Y ==> M2, en revenant à origine

                # ### SI RIEN NE MARCHE : 
                # ### Reexprimer point à point, en déduire matrice rotation

                # # twist_used = twist_used1e400
                # # twist_used = twist_used*1e-400

                # print(twist_used)
                # ## try to get newM
                # M2 = self.tU.L2M(L2)
                # ### STRAT 1

                # Lrpy = self.tU.M2Lrpy(M2)

                # # print(Lrpy)
                # # exit()

                # # rX = Lrpy[3] + twist_used[0]
                # # rY = Lrpy[4] + twist_used[1]
                # # rZ = Lrpy[5] + twist_used[2]

                # ## calculer R 0 - > 1
                # # R_g = self.tU.Lrpy2M([0.0,0.0,0.0,Lrpy[3]+twist_used[0],0.0,0.0])
                # # R_b = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,Lrpy[4]+ twist_used[1],0.0])
                # # R_a = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,Lrpy[5]+twist_used[2]])

                # ## ok
                # R_g = self.tU.Lrpy2M([0.0,0.0,0.0,Lrpy[3],0.0,0.0])
                # R_a = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,Lrpy[4],0.0])
                # R_b = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,Lrpy[5]])

                # ## change rien
                # # R_g = self.tU.Lrpy2M([0.0,0.0,0.0,Lrpy[3]+twist_used[0],0.0,0.0])
                # # R_a = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,Lrpy[4]+twist_used[1],0.0])
                # # R_b = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,Lrpy[5]+twist_used[2]])

                # # R_g = self.tU.Lrpy2M([0.0,0.0,0.0,twist_used[0],0.0,0.0])
                # # R_b = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,twist_used[1],0.0])
                # # R_a = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,twist_used[2]])



                # R_0_1 = R_g
                # R_1_2 = np.dot( np.dot( np.transpose(R_0_1), R_b), R_0_1)
                
                # R_0_2 = np.dot(R_0_1, R_1_2)
                
                # R_2_t = np.dot( np.dot( np.transpose(R_0_2), R_a), R_0_2)

                # newM_rG= np.dot( np.dot(R_0_1,R_1_2), R_2_t)

                # print(M_dt)

                # print(M2)
                # print(newM_rG)

                # #newM = np.dot(M, newM_rG)

                # #exit()

                # ## Si lRPY contient twist
                # #newM_rG2 = newM_rG

                # newM_rG2 = np.dot(newM_rG,M_dt)

                # print("x : ", R_g)
                # print("y : ", R_a)
                # print("z : ", R_b)

                # # print(newM_rG2)

                # ## ok mais local
                # newM = np.dot(M, newM_rG2)

                # ## chaotique :(
                # #newM = np.dot(newM_rG2,M2)


                ## interestingly, creates a speed control for up/Down
                # elif feedback.control_name == "rotate_y_fixed":
                #     ## marche, mais à terminer
                #     L_reset,suc = self.tU.getPose(self.refG_marker,self.refCubeName)
                #     L2,suc = self.tU.getPose(self.refCubeTrans,self.vialName)

                #     #L_transfer,suc = self.tU.getPose(self.b)

                #     if suc:
                #         ## ==> refCubeNameMoving ds refG_marker
                #         newMdes_0_frame = self.tU.L2M(L) 

                #         newM = np.dot( newMdes_0_frame,self.tU.L2M(L2))



                #         for i in range(3):
                #             newM[i,3] = 0.0
                #         Pori = self.tU.M2P(newM)

                #         ## P : for server

                #         MendW = newM
                #         Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
                #         P = self.tU.M2P(Mreal)

                #         #exit()
                #         suc = self.server.setPose(self.vialName, P)
                #         if suc:
                #             suc = self.setPoseInRef(self.vialName,self.tU.P2T(Pori), self.refG_marker)  
                                                

                #         ## reset marker at origin pose (to prevent any displacement of the axis)
                #         suc = self.server.setPose(self.refCubeTrans, self.tU.L2P(L_reset))
                #         self.setPoseInRef(self.refCubeTrans,self.tU.L2T(L_reset), self.refG_marker)



    # def setVialPoseViaTwist(self):
    #     """!
        
    #     x : blue axis
    #     y : green axis
    #     z : red axis

        
    #     """
    #     twist_used = copy.deepcopy(self.twist)
    #     ## - Exprimer twist de local à world (via matrice adjointe, voir https://github.com/NxRLab/ModernRobotics/blob/master/packages/MATLAB/mr/Adjoint.m)
    #     ##      * nu_world = [Ad_T[world->loc[k]]]*nu_loc = Adjoint(T^loc[k]_world)  * nu_loc
    #     ## - Appliquer twist_world à Tk_w
    #     ##      * Tloc[k+1]_world = T^loc[k]_world * e(nu_world) = T^loc[k]_world * se3mat(nu_world)

    #     #self.twistDebug = True
    #     upDownMvt = False

    #     ## x
    #     isv = 0
    #     ## y
    #     #isv = 1
    #     ## z
    #     #isv = 2  



    #     if self.twistDebug:                      
        
    #         for i in range(5):
    #             if (i != isv):
    #                 twist_used[i] = 0.0
    #             else:
    #                 sign = 1
    #                 if self.asPolar and isv == 0:
    #                     sign = np.sign(twist_used[i])
    #                 twist_used[i] = sign* self.cMax * self.twistSensibility 
                

    #     L = []
    #     L2 = []
    #     # L3 = []
    #     # suc = False
    #     # if not self.asPolar:
    #     if self.asPolar:
    #         #print(twist_used)

    #         if twist_used[2] != 0:
    #             ## Z-axis of vial case
    #             self.isTwistLocal = True

    #         elif twist_used[1] != 0:
    #             ## left-right
    #             if self.isSim:
    #                 twist_used[2] = twist_used[1]
    #             else:
    #                 twist_used[2] = twist_used[1]
    #             twist_used[1] = 0

    #             self.isTwistLocal = False
    #         else:
    #             ## upDown
    #             ## classic 3d mouse : 
    #             ## pressToDown (hand on up side, go to down) ==> positive ==> should go down
    #             ## pressToUp (hand on down side, go to up) ==> negative ==> should go up
    #             if self.isSim:
    #                 twist_used[1] = -twist_used[0]
    #             else:
    #                 twist_used[1] = -twist_used[0]                    
    #             twist_used[0] = 0

    #             ## desactivated for now
    #             #twist_used[1] = 0

    #             upDownMvt = True

    #             self.isTwistLocal = False
    #     # print(twist_used)
    #     # print("===")

    #     if not self.isTwistLocal or upDownMvt:
    #         L,suc = self.tU.getPose(self.originFrame,self.refCubeName)
    #         L2,suc = self.tU.getPose(self.refCubeName,self.vialName)
    #     else:
    #         L,suc = self.tU.getPose(self.originFrame,self.vialName)
    #     # else:
    #     #     L,suc = self.tU.getPose(self.originFrame,self.refCubeName)
    #     #     L2,suc = self.tU.getPose(self.refCubeName,self.vialName)

    #     if suc:
    #         # self.twist = np.array([0,0,1e-4,0,0,0])

  
            
    #         M = self.tU.L2M(L)

    

    #         #print("got twist : ", self.twist)
                    

    #         # M_inServer = M @ np.linalg.inv(self.serv2TFTransf) 
    #         # M = M_inServer
          
    #         ## STRAT 4

    #         ## twist in ref_G
    #         twist_XYZ = copy.deepcopy(twist_used)


    #         M_dt = mr.MatrixExp6( mr.VecTose3(twist_XYZ) )

    #         ## obtenir nouvelle pose de vial dans ref_G, sachant twist M_dt
    #         newM = np.identity(4)


    #         if upDownMvt :
    #             M2 = self.tU.L2M(L2)                
    #             ## special polar case. Steps : 
    #             ## M : refTF ds oriF
    #             ## M2 : vN ds refTF

    #             ## 0 (at init) : déf vecteur V_init sur plan X-Y : orienté suivant
    #             ## Y positif 
    #             if self.recomputeTht:

    #                 ## 1 : obtenir projeté d'axe de fiole sur plan X-Y : aF_cur
    #                 axis = np.zeros((4,1))
    #                 axis[2] = 1.0
    #                 axis[3] = 1.0

    #                 P = np.dot(M2, axis)
                    
    #                 ## M2 n'a pas de terme de translation
    #                 ##print(M2)
    #                 # #print(M2)
    #                 #print(P)

    #                 normal = np.zeros((4,))
    #                 normal[2] = 1.0
    #                 normal[3] = 1.0

    #                 pt = np.zeros((3,))    

    #                 # print(pt)
    #                 # print(normal)            
    #                 R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
                    
                    
    #                 P_XY = np.dot(R,P)

    #                 #P_XY = np.array([P[0,0], P[1,0], 0.0, 1.0])
    #                 secAxis = np.array([0.0,1.0,0.0])
    #                 thirdAxis = np.array([0.0,0.0,1.0])
    #                 #P_XY = P

    #                 #print(P_XY)
    #                 ## debug only
    #                 #tht = 0


    #                 #self.V_init = np.array([1.0,0.0,0.0])
    #                 #if False:
    #                 #print(self.isSim)
                    
    #                 if np.linalg.norm(P_XY[:3]) < 1e-6 and self.rebootToTf:
    #                     ## permet de régler si au départ, on préfère que fiole aille
    #                     ## avec pointe à gauche ou pointe à droite

    #                     #tht = 1e-6
    #                     if not self.begToRight:
    #                         #print("beg_to_right")
    #                         tht = np.pi #1e-6

    #                     else:
    #                         #
    #                         #tht = -1e-6
    #                     #     #print("not sim")
    #                         #tht = np.pi
    #                         tht = 0.0
    #                     #print(tht)

    #                 else:
                        
    #                     ## 2 : obtenir angle entre aF_cur / V_init
    #                     tht = np.arccos( np.dot(self.V_init,P_XY[:3])/ np.linalg.norm(P_XY[:3] ) )
                        
    #                     tht = tht[0]

                        
    #                     # elif (np.dot(secAxis,P_XY[:3]) ) <0:
    #                     #     tht = -tht
                        

    #                     val = np.dot(thirdAxis, P[:3] )
    #                         # if (np.dot(secAxis, P[:3] ))<0:
                        
    #                         # tht = 2*np.pi - tht 

    #                     ## corrige pbme d'exp sur [0, pi uniquement]

    #                     if (np.dot(secAxis, P_XY[:3] )) <0:
    #                         tht = 2*np.pi - tht
    #                         #tht = -np.pi+tht 

    #                 while tht >= 2*np.pi:
    #                     print("here")
    #                     tht = tht - 2*np.pi

    #                 self.PrimAxisVialSign = np.sign( np.dot(self.V_init,P_XY[:3]) )

    #                 self.tht = tht
    #                 #print("tht selected : ", self.tht)
    #                 self.recomputeTht = False

    #                 tht_ori = tht
    #                 #print("tht ori : ", tht_ori)

    #                 M_rot_ori = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,tht_ori])

    #                 ## display

    #                 T2 = self.tU.M2T(M_rot_ori)

    #                 # tht ok, goto twist
    #                 transf =  geometry_msgs.msg.TransformStamped()
    #                 transf.child_frame_id = "debug_ori"
    #                 transf.header.frame_id = self.refCubeName
    #                 transf.header.stamp = rospy.Time.now()
    #                 transf.transform = T2
    #                 self.tU.tfToStaticTS(transf)

    #                 ## test
    #                 #self.recomputeTht = True

    #             #if not self.begToRight and self.tht < np.pi/2:
    #             if not self.begToRight and self.tht >= np.pi:
    #                 #twist_used[1] = -twist_used[1]
    #                 ## tht = 0.17 ==> pas up-down si changeSign
    #                 twist_used[1] = -twist_used[1]
    #                 M_dt =  mr.MatrixExp6( mr.VecTose3(twist_used) )
                

    #             if self.tht > np.pi :
    #                 #print("tht sup")
    #                 if self.isSim:
    #                     twist_used[1] = twist_used[1]
    #                 else:
    #                     twist_used[1] = -twist_used[1]

    #                 M_dt =  mr.MatrixExp6( mr.VecTose3(twist_used) )
                
                
    #             ## suppressed, not useful anymore
    #             # if np.sign(val) > 0 :
    #             #     if tht < np.pi/2:
    #             #         tht = np.pi - tht
    #             #     # elif (np.dot(thirdAxis, P[:3] ))<0:
    #             #     #     tht = -tht
    #             #     elif (np.dot(secAxis,P_XY[:3]) ) >0:
    #             #         tht = -tht                        
    #             # elif np.sign(val) < 0:
    #             #     if tht < np.pi/2:
    #             #         tht = np.pi - tht
    #             #     elif (np.dot(secAxis,P_XY[:3]) ) >0:
    #             #         tht = -tht     

    #             tht = self.tht




                 
    #             # tht = np.sign(tht)*(abs(tht)%np.pi)                   
    #             # print(val)
    #             # print(tht)

                
    #             ## 3 : faire + 90 de cet angle dans sens trigo : on obtient V_cur 
                


    #             ## 4 : créer réf ref_G_special, correspondant à ref_G mais tq V_cur = axe X ref_G_S
    #             ## (devrait normalement correspondre à rotation de ref_G de angle suivant axe Z de ref_G_S)
    #             M_rot = self.tU.Lrpy2M([0.0,0.0,0.0,0.0,0.0,tht])

    #             ## display
    #             T = self.tU.M2T(M_rot)

    #             transf =  geometry_msgs.msg.TransformStamped()
    #             transf.child_frame_id = "debug_disp_rot_axis_Y"
    #             transf.header.frame_id = self.refCubeName
    #             transf.header.stamp = rospy.Time.now()
    #             transf.transform = T
    #             self.tU.tfToStaticTS(transf)

    #             ## M : refTF ds oriF
    #             ## M2 : vN ds refTF


    #             ## 5 : appliquer twist en global : 


    #             MrefTf2_oriTF = np.dot(M, M_rot)
    #             # print(M)
    #             # print(MrefTf2_oriTF)

    #             ## b : appliquer M_dt à ce ref
    #             # newMrefTf2_oriTF = np.dot(MrefTf2_oriTF, M_dt)

    #             MVN_refTf2 = np.dot( np.linalg.inv(M_rot), M2)

    #             ## c : go back
    #             # newM_rG = np.dot(M, M_dt)
    #             # newM = np.dot(newM_rG, M2)

    #             newM = np.dot( np.dot( MrefTf2_oriTF , M_dt), MVN_refTf2 )

    #             # newM = np.dot(newMrefTf2_oriTF, M2 )

    #             # ## a : appliquer M_dt à M_rot

    #             # newM_dt = np.dot(M_rot,M_dt) 

    #             # newM_rGS = np.dot(M, newM_dt)

    #             # newM = np.dot(np.dot( np.linalg.inv(M_rot) ,newM_rGS), M2 )


    #             #newM = np.dot( np.dot( newM_rgS, np.linalg.inv(M_rot) ) , M2 )

    #             ##  - newM_rG_S = np.dot(M_rG_S,M_dt)
    #             ##  - newM = newM_RGS * Tref_RGS-> refG * M2

    #             ## for now, for debug
    #             # print(("=="))
    #             # newM_rG = np.dot(M, M_dt)
    #             # newM = np.dot(newM_rG, M2)

    #             #if np.linalg.norm(P_XY[:3]) > 1e-5 :
    #             if self.rebootToTf:
    #                 self.rebootToTf = False


    #         elif not self.isTwistLocal:
    #             #print(L2)
    #             ## M2 : midVal[k] dans ref_G
    #             M2 = self.tU.L2M(L2)
    #             # ## newM_rG : midVial[k+1] dans ref_G
    #             # newM_rG = np.dot(M2,M_dt)
    #             # print(self.tU.M2L(M_dt) )
    #             # print( self.tU.M2L(newM_rG) )
    #             # print("==")
    #             #print(twist_used)

    #             ## faire comme si on déplaçait ref_G
    #             newM_rG = np.dot(M, M_dt)
    #             newM = np.dot(newM_rG, M2)

    #             #print("recompute tht because of left-right mvt")

    #             self.recomputeTht = True


    #         else:
    #             ## Obtenir midVial[k+1] ds world à partir de refG dans world et midVial[k+1] dans refG
    #             newM = np.dot(M, M_dt)


    #         ## Appliquer pour avoir newM

    #         # newM = np.dot(M, M_dt)

 
    #         for i in range(3):
    #             newM[i,3] = self.posOri[i]
 

    #         ## FINAL : (newM should be T^[midVal[k+1]]_[world])

    #         ## Pori : for /TF
    #         Pori = self.tU.M2P(newM)

    #         ## P : for server

    #         MendW = newM
    #         Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
    #         P = self.tU.M2P(Mreal)

    #         #exit()
    #         suc = self.server.setPose(self.vialName, P)
    #         if suc:
    #             suc = self.setVialPose(Pori)     
            

    #     if self.asPolar:
    #         self.isTwistLocal = False
    #         #print("////")


    # def arrowUpdate(self,M):
    #     """!
    #     A partir d'une orientation de la TF associée à la fiole
    #     (R^{Dk}_{A}), réoriente les différents marqueurs (cube, arrow) et les TF associés
    #     (donc : R^{Bk}_{A}, R^{Ck}_{A} )
        
    #     Avec : 
    #         * M : R^{Dk}_{vialRG}, la nouvelle orientation de la flèche
    #         (exprimée dans le repère vialRG)

    #     Énormes bugs lorsqu'on est proche des singularités... mis de côté pour le moment.
        
    #     """
    #     ## first, turn back M to R^{Dk}_{A}
    #     ## (for all future calculus)
    #     R_Dk_A = self.M_vialRG_A @ M

    #     if self.fixed:
    #         ## pas de composante en translation
    #         for k in range(3):
    #             R_Dk_A[k,3] = 0.0

    #     ## deb : vérifie prés de singularité
    #     beta_k = 0
    #     R_Bk_A = np.identity(4)
    #     R_Ck_Bk = np.identity(4)
    #     self.inputCheck=False

    #     singCase = False
    #     #print(R_Dk_A[2,2])
    #     if abs( abs(R_Dk_A[2,2]) - 1) < self.thresSing:
    #         print("singularity case")
    #         #self.inputCheck=True
    #         ## singularity case : sin(beta) = 0
    #         ## on fixe R^{Bk}_{A} = R^{Bk-1}_{A}, afin d'éviter une
    #         ## modification involontaire du cercle vert
    #         # suc = False
    #         # ## R^{B_{k-1}}_{A}
    #         # L_Bk1_A = []
    #         # suc_Bk1_A = False

    #         # # axis = np.zeros((3,1))
    #         # # axis[2] = 1.0
    #         # # self.sign_sinbk = np.sign(np.dot(R_Dk_A[:3,2],axis)) 


    #         # while not rospy.is_shutdown() and not suc:
    #         #     L_Bk1_A,suc_Bk1_A = self.tU.getPose(self.refG_marker, self.refCubeLong)

    #         #     suc = (suc_Bk1_A)
            
    #         # if suc:

    #         #     R_Bk_A = self.tU.L2M(L_Bk1_A)

    #         #     beta_k = np.arccos(R_Dk_A[2,2])
    #         #     # print(R_Dk_A[2,2])

    #         #     #print(beta_k)

    #         #     R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
    #         #                         [0.0, 1.0, 0.0,0.0],
    #         #                         [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
    #         #                         [0.0, 0.0, 0.0, 1.0]                        
    #         #                         ])
    #         #     # print(R_Ck_Bk)
    #         #     # input()

    #         ## poss 2 : on fait en sorte que R^{Dk}_{Ck} = R^{Dk-1}_{Ck-1}
    #         ## (e.g on annule toute modification de l'axe de la fiole)
    #         ## risques d'instabilité à singularité :( 
    #         suc = False
    #         ## R^{B_{k-1}}_{A}
    #         L_Dk1_Ck1 = []
    #         suc_Dk1_Ck1 = False

    #         # axis = np.zeros((3,1))
    #         # axis[2] = 1.0
    #         # self.sign_sinbk = np.sign(np.dot(R_Dk_A[:3,2],axis)) 


    #         while not rospy.is_shutdown() and not suc:
    #             L_Dk1_Ck1,suc_Dk1_Ck1 = self.tU.getPose(self.refCubeTrans, self.vialName)

    #             suc = (suc_Dk1_Ck1)
            
    #         if suc:

    #             ## 1 : obtenir R_Bk_A, avec projeté.
    #             normal = np.zeros((4,))
    #             normal[2] = 1.0
    #             normal[3] = 1.0

    #             pt = np.zeros((3,))    
            
    #             R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
                
    #             R_Dk_A_XY = np.dot(R,R_Dk_A)

    #             ## position axe Z sur plan X-Y (= pointe de fiole)
    #             R_Dk_A_XY_axeZ = R_Dk_A_XY[:3,2]

    #             print("Z sur plan X-Y : ", R_Dk_A_XY_axeZ)
    #             if np.max([abs(R_Dk_A_XY_axeZ[0]), abs(R_Dk_A_XY_axeZ[1])] ) < self.thresCompute:
    #                 print("special sing case")
    #                 ## R_Bk_A = R_Bk1_A
    #                 suc = False
    #                 ## R^{B_{k-1}}_{A}
    #                 L_Bk1_A = []
    #                 suc_Bk1_A = False

    #                 # axis = np.zeros((3,1))
    #                 # axis[2] = 1.0
    #                 # self.sign_sinbk = np.sign(np.dot(R_Dk_A[:3,2],axis)) 


    #                 while not rospy.is_shutdown() and not suc:
    #                     L_Bk1_A,suc_Bk1_A = self.tU.getPose(self.refG_marker, self.refCubeLong)

    #                     suc = (suc_Bk1_A)
                    
    #                 if suc:

    #                     R_Bk_A = self.tU.L2M(L_Bk1_A)

    #                     alpha_k = np.arctan2( R_Bk_A[1,0], R_Bk_A[0,0]  )

    #                     beta_k = np.arctan2(R_Dk_A[0,2]/np.cos(alpha_k), R_Dk_A[2,2] )

    #                     # if np.cos(alpha_k) != 0.0:
    #                     #     sin_betak = R_Dk_A[0,2]/np.cos(alpha_k)
    #                     # else:
    #                     #     sin_betak = R_Dk_A[1,2]/np.sin(alpha_k)
    #                     # print("sin_betak : ", sin_betak)
    #                     # beta_k = np.arcsin(sin_betak)
    #                     sin_betak = np.sin(beta_k)
    #                     eps_k =  np.arctan2( R_Dk_A[2,1]/sin_betak, -R_Dk_A[2,0]/sin_betak  )
    #                     R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
    #                                         [0.0, 1.0, 0.0,0.0],
    #                                         [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
    #                                         [0.0, 0.0, 0.0, 1.0]                        
    #                                         ])

    #             else:
    #                 print("not special sing case")
    #                 prod_scal = np.dot(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
    #                 prod_vec = np.cross(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
                    
    #                 print(prod_vec)
    #                 ## angle
    #                 ## sinus issu de : http://villemin.gerard.free.fr/aMaths/GeomAnal/SinusPV.htm
    #                 ## cos issu de : https://fr.wikihow.com/calculer-l%E2%80%99angle-entre-deux-vecteurs
    #                 cos = prod_scal/np.linalg.norm(R_Dk_A_XY_axeZ)  
    #                 sin = np.linalg.norm(prod_vec)/np.linalg.norm(R_Dk_A_XY_axeZ) 

    #                 print("cos : ", cos)
    #                 print("sin : ", sin) 
                    
    #                 sign = np.sign(R_Dk_A_XY_axeZ[1] )

    #                 tht2 = np.arctan2(-sin,cos)
    #                 tht = np.arctan2(R_Dk_A_XY[1,0],R_Dk_A_XY[0,0])
    #                 tht3 = np.arctan2(-R_Dk_A_XY[1,2],R_Dk_A_XY[0,2])
    #                 print("tht (col 1):",tht)
    #                 print("tht (sin,cos) : ", tht2)
    #                 print("tht (col 3): ", tht3)
                    
    #                 alpha_k = tht2
    #                 # print(alpha_k)

    #                 R_Bk_A = np.array([[np.cos(alpha_k), -np.sin(alpha_k), 0.0,0.0],
    #                                     [np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
    #                                     [0.0, 0.0, 1.0, 0.0],
    #                                     [0.0, 0.0, 0.0, 1.0]                                                               
    #                                     ])
            
    #                 R_Ck_Bk = np.linalg.inv(R_Bk_A) @ R_Dk_A @ np.linalg.inv( self.tU.L2M(L_Dk1_Ck1) )


    #             # # ## arccos : pas de signe, pbme.
    #             # # ## faire plutôt projeté. 
    #             # beta_k = np.arccos(R_Dk_A[2,2])
    #             # # print(R_Dk_A[2,2])

    #             # #print(beta_k)

    #             # R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
    #             #                     [0.0, 1.0, 0.0,0.0],
    #             #                     [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
    #             #                     [0.0, 0.0, 0.0, 1.0]                        
    #             #                     ])
            
    #             # R_Bk_A = R_Dk_A @ np.linalg.inv( self.tU.L2M(L_Dk1_Ck1) ) @ np.linalg.inv(R_Ck_Bk)
                
    #             # # print(R_Ck_Bk)
    #             # # input()
    #     else:

    #         # L_sign_sinBK = []

    #         # ## test 1 : on essaye les 2 poss, on voit si on arrive ou non à la même chose
    #         # L_sign_sinBK = [1, -1]
    #         # ## ==> pas un critère, renvoie les mêmes erreurs quelque soit signe.
    #         # ## test 2 : via une valeur fixée
    #         # axis = np.zeros((3,1))
    #         # axis[2] = 1.0
    #         # # L_sign_sinBK = [ np.sign(np.dot(R_Dk_A[:3,2],axis)) ]

    #         # ## poss 3:

    #         # L_sign_sinBK = [1]
    #         # # L_sign_sinBK = [-1]          
    #         # # 
    #         # # poss 4 :
    #         # #L_sign_sinBK = [self.sign_sinbk]
    #         # best_err = 1e6
    #         # best_alphak = None
    #         # best_sign = None

    #         # for l in range(len(L_sign_sinBK)):
    #         #     sign_sinBK = L_sign_sinBK[l]

    #         #     bk = np.arctan2( sign_sinBK*np.sqrt( (R_Dk_A[0,2])**2 + (R_Dk_A[1,2])**2 )  ,  R_Dk_A[2,2] )
    #         #     try:
    #         #         beta_k = bk[0]
    #         #     except IndexError:
    #         #         beta_k = bk

    #         #     ## correct representation : http://webserver2.tecgraf.puc-rio.br/~mgattass/demo/EulerAnglesRotations-GimballLock/euler.html
    #         #     ## !! np.arctan2(y,x)
    #         #     sin_betak = np.sin(beta_k)

    #         #     alpha_k = np.arctan2( R_Dk_A[1,2]/sin_betak, R_Dk_A[0,2]/sin_betak  )

    #         #     eps_k =  np.arctan2( R_Dk_A[2,1]/sin_betak, -R_Dk_A[2,0]/sin_betak  )

    #         #     R_Bk_A = np.array([[np.cos(alpha_k), -np.sin(alpha_k), 0.0,0.0],
    #         #                         [np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
    #         #                         [0.0, 0.0, 1.0, 0.0],
    #         #                         [0.0, 0.0, 0.0, 1.0]                                                               
    #         #                         ])
                
    #         #     R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
    #         #                         [0.0, 1.0, 0.0,0.0],
    #         #                         [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
    #         #                         [0.0, 0.0, 0.0, 1.0]                        
    #         #                         ])
            
    #         #     R_Dk_Ck = np.array([[np.cos(eps_k), -np.sin(eps_k), 0.0,0.0],
    #         #                         [np.sin(eps_k), np.cos(eps_k), 0.0, 0.0],
    #         #                         [0.0, 0.0, 1.0, 0.0],
    #         #                         [0.0, 0.0, 0.0, 1.0]                                                               
    #         #                         ])
    #         #     # print(beta_k)
    #         #     # print(M)
    #         #     # print(R_Dk_Ck)

                
    #         #     matErr = R_Bk_A @ R_Ck_Bk @ R_Dk_Ck @ np.linalg.inv(R_Dk_A)
                
    #         #     err = np.linalg.norm(matErr - np.identity(4))

    #         #     # print("For sign %d , err : %3.4f"%(sign_sinBK,err))
    #         #     # print("result Mat : ", self.tU.M2Lrpy(matErr))
    #         #     # print(matErr)

    #         #     if err < best_err:
    #         #         best_err = err
    #         #         best_alphak = alpha_k
    #         #         best_sign = L_sign_sinBK[l]

    #         # # print("best sign :", best_sign)
    #         # alpha_k = best_alphak
    #         # R_Bk_A = np.array([[np.cos(alpha_k), np.sin(alpha_k), 0.0,0.0],
    #         #                     [-np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
    #         #                     [0.0, 0.0, 1.0, 0.0],
    #         #                     [0.0, 0.0, 0.0, 1.0]                                                               
    #         #                     ])
    #         ## Poss 5 :
    #         ## - Obtenir R^{Bk}_{A} à partir du projeté
    #         ## de R^{Dk}_{A} sur le plan (X-Y)
    #         ## - Grâce à R^{Bk}_{A}, en déduire alpha_k,
    #         ## donc sin(beta_k)

    #         ## instabilité lorsque x_Bk = z_Dk

    #         print("not sing")
    #         ## OK EN DEHORS DE SINGULARITÉ, ON NE TOUCHE PLUS!

    #         normal = np.zeros((4,))
    #         normal[2] = 1.0
    #         normal[3] = 1.0

    #         pt = np.zeros((3,))    
        
    #         R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
            
    #         R_Dk_A_XY = np.dot(R,R_Dk_A)

    #         ## position axe Z sur plan X-Y (= pointe de fiole)
    #         R_Dk_A_XY_axeZ = R_Dk_A_XY[:3,2]/np.linalg.norm( R_Dk_A_XY[:3,2])
    #         print("Z sur plan X-Y : ", R_Dk_A_XY_axeZ)
    #         prod_scal = np.dot(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
    #         prod_vec = np.cross(np.array([1.0,0.0,0.0]), R_Dk_A_XY_axeZ)
            
    #         print(prod_vec)
    #         ## angle
    #         ## sinus issu de : http://villemin.gerard.free.fr/aMaths/GeomAnal/SinusPV.htm
    #         ## cos issu de : https://fr.wikihow.com/calculer-l%E2%80%99angle-entre-deux-vecteurs
    #         cos = prod_scal/np.linalg.norm(R_Dk_A_XY_axeZ)  
    #         sin = np.linalg.norm(prod_vec)/np.linalg.norm(R_Dk_A_XY_axeZ) 

    #         sign = np.sign(R_Dk_A_XY_axeZ[1] )

    #         #tht2 = np.arctan2(-sin,cos)
    #         ## ==> OK, STABLE
    #         tht2 = np.arctan2(sign*sin,cos)
    #         tht = np.arctan2(R_Dk_A_XY[1,0],R_Dk_A_XY[0,0])
    #         tht3 = np.arctan2(-R_Dk_A_XY[1,2],R_Dk_A_XY[0,2])
    #         print("tht (col 1):",tht)
    #         print("tht2 (sin, cos) : ", tht2)
    #         print("tht3 (col 3) : ", tht3)

    #         #     tht = tht2
                
    #         # print("tht:",tht)
    #         # print("tht2 : ", tht2)
    #         # print(R_Dk_A_XY[1,0]," , ", R_Dk_A_XY[0,0])
            
    #         # print("cos_betak : ", R_Dk_A[2,2])
    #         # #if R_Dk_A[2,2]< self.thresSing:
    #         # print("projeté : ",R_Dk_A_XY[:3,:3])


    #         # print("tht:",tht)
    #         alpha_k = tht2
    #         # print(alpha_k)

    #         R_Bk_A = np.array([[np.cos(alpha_k), -np.sin(alpha_k), 0.0,0.0],
    #                             [np.sin(alpha_k), np.cos(alpha_k), 0.0, 0.0],
    #                             [0.0, 0.0, 1.0, 0.0],
    #                             [0.0, 0.0, 0.0, 1.0]                                                               
    #                             ])
            
    #         # abs_sin_bk = np.sqrt( (R_Dk_A[1,2])**2 + (R_Dk_A[0,2])**2)
    #         # L_sign = [-1,1]
    #         # err = 1e6
    #         # sign = None
    #         # for k in range(len(L_sign)):
    #         #     sin_bk = abs_sin_bk*L_sign[k]
    #         #     alpha_k2 = np.arctan2(R_Dk_A[1,2]/sin_bk, R_Dk_A[0,2]/sin_bk)
    #         #     print("sign : %d ; alphak2 - alpha_k :"%(L_sign[k]), alpha_k2-alpha_k)
    #         #     if (abs(alpha_k2 - alpha_k) < err):
    #         #         err = alpha_k2 - alpha_k
    #         #         sign = L_sign[k]
    #         # beta_k = np.arcsin(sign*abs_sin_bk)

    #         beta_k = np.arctan2(R_Dk_A[0,2]/np.cos(alpha_k), R_Dk_A[2,2] )
    #         print("beta_k : ", beta_k)
    #         # if np.cos(alpha_k) != 0.0:
    #         #     sin_betak = R_Dk_A[0,2]/np.cos(alpha_k)
    #         # else:
    #         #     sin_betak = R_Dk_A[1,2]/np.sin(alpha_k)
    #         # print("sin_betak : ", sin_betak)
    #         # beta_k = np.arcsin(sin_betak)
    #         sin_betak = np.sin(beta_k)
    #         eps_k =  np.arctan2( R_Dk_A[2,1]/sin_betak, -R_Dk_A[2,0]/sin_betak  )
    #         R_Ck_Bk = np.array([[np.cos(beta_k), 0.0, np.sin(beta_k),0.0],
    #                             [0.0, 1.0, 0.0,0.0],
    #                             [-np.sin(beta_k), 0.0, np.cos(beta_k),0.0],    
    #                             [0.0, 0.0, 0.0, 1.0]                        
    #                             ])
            
    #         # print("R_Ck_Bk : ", R_Ck_Bk)
        
    #         R_Dk_Ck = np.array([[np.cos(eps_k), -np.sin(eps_k), 0.0,0.0],
    #                             [np.sin(eps_k), np.cos(eps_k), 0.0, 0.0],
    #                             [0.0, 0.0, 1.0, 0.0],
    #                             [0.0, 0.0, 0.0, 1.0]                                                               
    #                             ])
    #         if R_Dk_A[2,2]< self.thresSing:
    #             print("sing - treatment")
    #             #input()
        
    #     # normal = np.zeros((4,))
    #     # normal[2] = 1.0
    #     # normal[3] = 1.0

    #     # pt = np.zeros((3,))    
    
    #     # R = tf.transformations.projection_matrix(pt,normal, pseudo = False)
        
    #     # P_XY = np.dot(R,R_Dk_A)
    #     # print("R_Dk_A sur X-Y : ", P_XY[:3,:3])

    #     # print("R_Bk_A : ", R_Bk_A[:3,:3])

    #     R_Dk_vialRG = np.linalg.inv(self.M_vialRG_A) @ R_Dk_A
    #     R_Ck_A = R_Bk_A @ R_Ck_Bk
    #     # print(R_Bk_A)
    #     # print(R_Ck_A)
    #     # input()
    #     ## now update everyone

    #     ## Bk
    #     self.markerAndRefUpdate(R_Bk_A,R_Ck_A,R_Dk_vialRG)
    #     # P = self.tU.M2P(R_Bk_A)
    #     # suc = False
    #     # while not rospy.is_shutdown() and not suc:
    #     #     suc = self.server.setPose(self.refCubeName, P)
    #     # if suc:
    #     #     suc = self.setPoseInRef(self.refCubeName,self.tU.M2T(R_Bk_A), self.refG_marker)

    #     #     ## Dk
    #     #     # MendW = R_Dk_vialRG
    #     #     MendW = M            
    #     #     Mreal = MendW @ np.linalg.inv(self.serv2TFTransf) 
    #     #     P = self.tU.M2P(Mreal)
    #     #     suc = False
    #     #     while not rospy.is_shutdown() and not suc:
    #     #         suc = self.server.setPose(self.vialName, P)
    #     #     if suc:
    #     #         suc = self.setPoseInRef(self.vialName,self.tU.M2T(R_Dk_vialRG), self.vialRG)

    #     #         ## Ck
    #     #         suc = self.setPoseInRef(self.refCubeTrans,self.tU.M2T(R_Bk_A), self.refG_marker)






if __name__ == "__main__":
    CIM()